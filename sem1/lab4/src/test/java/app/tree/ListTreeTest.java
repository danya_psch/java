package app.tree;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ListTreeTest {

    @Test
    void put() {
        ListTree<String, Integer> container = new ListTree<>();

        container.put("key1", 1);
        container.put("key2", 0);
        container.put("key3", -1);

        assertEquals(1, container.get("key1"));
        assertEquals(0, container.get("key2"));
        assertNotEquals(-1, container.get("key4"));
    }

    @Test
    void isEmpty() {
        ListTree<String, Integer> container = new ListTree<>();
        assertTrue(container.isEmpty());
        container.put("key1", 1);
        assertFalse(container.isEmpty());
    }

    @Test
    void remove() {
        ListTree<String, Integer> container = new ListTree<>();

        container.put("key1", 1);
        container.put("key2", 0);
        container.put("key3", -1);

        container.remove("key2");
        assertFalse(container.contains("key2"));
        container.remove("key3");
        assertFalse(container.contains("key3"));
        assertTrue(container.contains("key1"));
    }

    @Test
    void size() {
        ListTree<String, Integer> container = new ListTree<>();

        container.put("key1", 1);
        container.put("key2", 0);

        assertEquals(2, container.size());
    }
}