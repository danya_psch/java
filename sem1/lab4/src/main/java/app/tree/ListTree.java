package app.tree;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class ListTree<K extends Comparable<K>,V> {

    public static class Pair<K, V> {
        K key;
        V value;

        public Pair(K key, V value) {
            this.key = key;
            this.value = value;
        }

    }



    final static boolean RED = true;
    final static boolean BLACK = false;

    public RBTreeNode<Pair<K, V>> NIL;
    private int M = 8;
    private boolean isTree = false;

    private int count = 0;
    private RBTreeNode<Pair<K, V>> root;
    private RBTreeNode<Pair<K, V>> tail;

    public ListTree() {
        NIL = new RBTreeNode<>(new Pair<>(null, null), null, null, BLACK);
        root = NIL;
    }

    public void put(K key,V value) {
        ++count;
        RBTreeNode<Pair<K, V>> newChild = new RBTreeNode<>(new Pair<>(key, value), NIL, NIL, RED);
        if (!isTree) {
            if (count < M) {
                if (root == NIL) {
                    root = newChild;
                } else {
                    tail.setLeft(newChild);
                }
                tail = newChild;
                return;
            } else {
                treeify();
            }
        }

        { //insert node in tree
            insert(newChild);
        }
    }

    private void insert(RBTreeNode<Pair<K, V>> newChild) {
        K key = newChild.getData().key;
        RBTreeNode<Pair<K, V>> curr = root;
        RBTreeNode<Pair<K, V>> parent = NIL;

        while (curr != NIL) {
            parent = curr;
            if (key.compareTo(curr.getData().key) < 0)
                curr = curr.getLeft();
            else
                curr = curr.getRight();
        }

        if (parent == NIL) {
            newChild.color = BLACK;
            root = newChild;
        } else if (key.compareTo(parent.getData().key) < 0) {
            parent.setLeft(newChild);
        } else {
            parent.setRight(newChild);
        }

        insertCase1(newChild);
    }

    private RBTreeNode<Pair<K, V>> grandparent(RBTreeNode<Pair<K, V>> child)
    {
        if (child != null && child.getParent() != null)
            return child.getParent().getParent();
        else
            return null;
    }

    private RBTreeNode<Pair<K, V>> uncle(RBTreeNode<Pair<K, V>> child)
    {
        RBTreeNode<Pair<K, V>> grandpa = grandparent(child);
        if (grandpa == null)
            return null;
        if (child.getParent() == grandpa.getLeft())
            return grandpa.getRight();
        else
            return grandpa.getLeft();
    }


    private void insertCase1(RBTreeNode<Pair<K, V>> node)
    {
        if (node.getParent() == null)
            node.color = BLACK;
        else
            insertCase2(node);
    }

    private void insertCase2(RBTreeNode<Pair<K, V>> node)
    {
        if (node.getParent().color != BLACK) {
            insertCase3(node);
        }
    }

    private void insertCase3(RBTreeNode<Pair<K, V>> node)
    {
        RBTreeNode<Pair<K, V>> uncle = uncle(node);
        RBTreeNode<Pair<K, V>> grandpa;

        if (uncle != null && uncle.color == RED) {
            // && (n->parent->color == RED) Второе условие проверяется в insert_case2, то есть родитель уже является красным.
            node.getParent().color = BLACK;
            uncle.color = BLACK;
            grandpa = grandparent(node);
            grandpa.color = RED;
            insertCase1(grandpa);
        } else {
            insertCase4(node);
        }
    }

    private void insertCase4(RBTreeNode<Pair<K, V>> node)
    {
        RBTreeNode<Pair<K, V>> grandpa = grandparent(node);

        if (node == node.getParent().getRight() && node.getParent() == grandpa.getLeft()) {
            leftRotate(node.getParent());
            node = node.getLeft();
        } else if (node == node.getParent().getLeft() && node.getParent() == grandpa.getRight()) {
            rightRotate(node.getParent());
            node = node.getRight();
        }
        insertCase5(node);
    }

    private void insertCase5(RBTreeNode<Pair<K, V>> node)
    {
        RBTreeNode<Pair<K, V>> grandpa = grandparent(node);

        node.getParent().color = BLACK;
        grandpa.color = RED;
        if (node == node.getParent().getLeft() && node.getParent() == grandpa.getLeft()) {
            rightRotate(grandpa);
        } else { /* (n == n->parent->right) && (n->parent == g->right) */
            leftRotate(grandpa);
        }
    }

    private void setParentChild(RBTreeNode<Pair<K, V>> node,
                                RBTreeNode<Pair<K, V>> pivot) {
        if (node.getParent() != null) {
            if (node.getParent().getLeft() == node)
                node.getParent().setLeft(pivot);
            else
                node.getParent().setRight(pivot);
        }
    }

    private void leftRotate(RBTreeNode<Pair<K, V>> node)
    {
        RBTreeNode<Pair<K, V>> pivot = node.getRight();
        pivot.setParent(node.getParent());

        if (root.equals(node)) {
            root = pivot;
        }
        setParentChild(node, pivot);
        node.setRight(pivot.getLeft());
        if (pivot.getLeft() != null) {
            pivot.getLeft().setParent(node);
        }
        node.setParent(pivot);
        pivot.setLeft(node);
    }

    private void rightRotate(RBTreeNode<Pair<K, V>> node)
    {
        RBTreeNode<Pair<K, V>> pivot = node.getLeft();
        pivot.setParent(node.getParent());

        if (root.equals(node)) {
            root = pivot;
        }
        setParentChild(node, pivot);
        node.setLeft(pivot.getRight());
        if (pivot.getRight() != null) {
            pivot.getRight().setParent(node);
        }
        node.setParent(pivot);
        pivot.setRight(node);
    }

    public V get(K key) {
        if (root == null) return null;
        if (!isTree) {
            RBTreeNode<Pair<K, V>> node = tail;
            while (node != null) {
                if (node.getData().key == key) {
                    return node.getData().value;
                }
                node = node.getParent();
            }
        } else {
            RBTreeNode<Pair<K, V>> curr = root;
            while (curr != NIL) {
                if (curr.getData().key.equals(key)) {
                    return curr.getData().value;
                }
                if (key.compareTo(curr.getData().key) < 0) {
                    curr = curr.getLeft();
                } else {
                    curr = curr.getRight();
                }
            }
        }
        return null;
    }
    public boolean isEmpty() {
        return root == NIL;
    }

    public V remove(K key) {
        --count;
        if (isTree) {
            if (count > M) {
                delete(key);
            } else {
                listify();
            }
        }

        {//remove from list
            RBTreeNode<Pair<K, V>> node = tail;
            while(node != null) {
                if (node.getData().key.equals(key)) {
                    RBTreeNode<Pair<K, V>> parent = node.getParent();
                    RBTreeNode<Pair<K, V>> child = node.getLeft();
                    if (root == node) {
                        root = child;
                        root.setParent(null);
                    }
                    if (tail == node) {
                        tail = parent;
                        tail.setLeft(NIL);
                    }
                    parent.setLeft(child);
                    return node.getData().value;
                }
                node = node.getParent();
            }
        }
        return null;
    }

    private RBTreeNode<Pair<K, V>> search(RBTreeNode<Pair<K, V>> node, K key)
    {
        if (node == NIL || node.getData().key.equals(key))
            return node;

        if (key.compareTo(node.getData().key) < 0)
            return search(node.getLeft(), key);

        return search(node.getRight(), key);
    }

    private V delete(K key) {
        RBTreeNode<Pair<K, V>> node = search(root, key);
        if (node.getLeft() != NIL && node.getRight() != NIL) {
            RBTreeNode<Pair<K, V>> minNode = minimum(node.getRight());
            node.setData(minNode.getData());
            deleteOneNode(minNode);
        } else {
            deleteOneNode(node);
        }
        return node.getData().value;
    }

    private RBTreeNode<Pair<K, V>> minimum(RBTreeNode<Pair<K, V>> node) {
        if (node != null) {
            RBTreeNode<Pair<K, V>> leftNode = node.getLeft();
            if (leftNode == NIL || leftNode == null) {
                return node;
            } else {
                return minimum(leftNode);
            }
        }
        return null;
    }

    public void replaceNode(RBTreeNode<Pair<K, V>> node, RBTreeNode<Pair<K, V>> child) {
        child.setParent(node.getParent());
        if (node == node.getParent().getLeft()) {
            node.getParent().setLeft(child);
        } else {
            node.getParent().setRight(child);
        }
    }

    private void deleteOneNode(RBTreeNode<Pair<K, V>> node) {
        RBTreeNode<Pair<K, V>> child = (node.getRight() == NIL) ? node.getLeft() : node.getRight();

        replaceNode(node, child);
        if (node.color == BLACK) {
            if (child.color == RED)
                child.color = BLACK;
            else
                deleteCase1(child);
        }
    }


    private void deleteCase1(RBTreeNode<Pair<K, V>> node) {
        if (node.getParent() != null) {
            deleteCase2(node);
        }
    }

    private void deleteCase2(RBTreeNode<Pair<K, V>> node) {
        RBTreeNode<Pair<K, V>> sibl = sibling(node);

        if (sibl.color == RED) {
            node.getParent().color = RED;
            sibl.color = BLACK;
            if (node == node.getParent().getLeft()) {
                leftRotate(node.getParent());
            } else {
                rightRotate(node.getParent());
            }
        }
        deleteCase3(node);
    }

    private void deleteCase3(RBTreeNode<Pair<K, V>> node) {
        RBTreeNode<Pair<K, V>> sibl = sibling(node);

        if (node.getParent().color == BLACK &&
                sibl.color == BLACK &&
                sibl.getLeft().color == BLACK &&
                sibl.getRight().color == BLACK) {
            sibl.color = RED;
            deleteCase1(node.getParent());
        } else
            deleteCase4(node);
    }

    private void deleteCase4(RBTreeNode<Pair<K, V>> node) {
        RBTreeNode<Pair<K, V>> sibl = sibling(node);

        if (node.getParent().color == RED &&
                sibl.color == BLACK &&
                sibl.getLeft().color == BLACK &&
                sibl.getRight().color == BLACK) {
            sibl.color = RED;
            node.getParent().color = BLACK;
        } else
            deleteCase5(node);
    }

    private void deleteCase5(RBTreeNode<Pair<K, V>> node) {
        RBTreeNode<Pair<K, V>> sibl = sibling(node);

        if  (sibl.color == BLACK) {
            if (node == node.getParent().getLeft() &&
                    sibl.getRight().color == BLACK &&
                    sibl.getLeft().color == RED) { /* this last test is trivial too due to cases 2-4. */
                sibl.color = RED;
                sibl.getLeft().color = BLACK;
                rightRotate(sibl);
            } else if (node == node.getParent().getRight() &&
                    sibl.getLeft().color == BLACK &&
                    sibl.getRight().color == RED) {/* this last test is trivial too due to cases 2-4. */
                sibl.color = RED;
                sibl.getRight().color = BLACK;
                leftRotate(sibl);
            }
        }
        deleteCase6(node);
    }

    private void deleteCase6(RBTreeNode<Pair<K, V>> node) {
        RBTreeNode<Pair<K, V>> sibl = sibling(node);

        sibl.color = node.getParent().color;
        node.getParent().color = BLACK;

        if (node == node.getParent().getLeft()) {
            sibl.getRight().color = BLACK;
            leftRotate(node.getParent());
        } else {
            sibl.getLeft().color = BLACK;
            rightRotate(node.getParent());
        }
    }

    private RBTreeNode<Pair<K, V>> sibling(RBTreeNode<Pair<K, V>> node)
    {
        if (node == node.getParent().getLeft())
            return node.getParent().getRight();
        else
            return node.getParent().getLeft();
    }

    public boolean contains(K key) {
        return get(key) != null;
    }
    public long size() {
        return count;
    }

    public void listify() {
        isTree = false;
        RBTreeNode<Pair<K, V>> tree = root;
        root = NIL;
        tail = root;
        depthFirstIteration(tree);
    }

    public void depthFirstIteration(RBTreeNode<Pair<K, V>> node) {
        if (node == NIL || node == null) return;
        if (tail == NIL) {
            root = node;
        } else {
            tail.setLeft(node);
        }
        tail = node;
        depthFirstIteration(node.getLeft());
        depthFirstIteration(node.getRight());
    }

    public void treeify() {
        isTree = true;
        RBTreeNode<Pair<K, V>> last_node = tail;
        root = NIL;

        while(last_node != null) {
            put(last_node.data.key, last_node.data.value);
            last_node = last_node.getParent();
        }
    }

    public void print() {
        if (!isTree) {
            RBTreeNode<Pair<K, V>> curr = tail;
            while(curr != null) {
                System.out.print(curr.getData().key + ", ");
                curr = curr.getParent();
            }
            System.out.println("");
        } else {
            printNode(root, 1);
        }
    }

    private void printNode(RBTreeNode<Pair<K, V>> node, int level) {
        if (node == null) return;

        StringBuilder str = new StringBuilder();
        for (int i = 0; i < level; ++i) {
            str.append("..");
        }

        System.out.println(str + (node.getData().key != null ? node.getData().key.toString() : "NIL") + " (" + (node.color == BLACK ? "BLACK" : "RED") + ")");
        printNode(node.getLeft(), level + 1);
        printNode(node.getRight(), level + 1);
    }



}




