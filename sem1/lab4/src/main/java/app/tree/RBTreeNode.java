package app.tree;

import java.util.List;

public class RBTreeNode<T> extends TreeNode<T> {
    boolean color;

    public RBTreeNode(T data, RBTreeNode<T> rightChild, RBTreeNode<T> leftChild, boolean color) {
        super(data);
        this.color = color;
        children.add(null);
        children.add(null);
        setLeft(leftChild);
        setRight(rightChild);
    }

    public RBTreeNode(T data, boolean color) {
        super(data);
        this.color = color;
    }

    @Override
    public RBTreeNode<T> removeChild(int index) {
        return (RBTreeNode<T>) super.removeChild(index);
    }

    public RBTreeNode<T> getLeft() {
        return (RBTreeNode<T>) getChild(0);
    }

    public void setLeft(RBTreeNode<T> newChild) {
        replaceChild(0, newChild);
    }

    public RBTreeNode<T> getRight() {
        return (RBTreeNode<T>) getChild(1);
    }

    public void setRight(RBTreeNode<T> newChild) {
        replaceChild(1, newChild);
    }

    public void replaceChild(int index, RBTreeNode<T> newChild) {
        children.set(index, newChild);
        if (newChild != null)
            newChild.parent = this;
    }

//    public RBTreeNode<T> replaceBy(RBTreeNode<T> node) {
//        RBTreeNode<T> parent = node.getParent();
//        if (node != null) {
//            node.setParent(parent);
//        }
//        if (this == this.getParent().getLeft()) {
//            parent.setLeft(node);
//        } else {
//            parent.setRight(node);
//        }
//        return this;
//    }

    @Override
    public RBTreeNode<T> getParent() {
        return (RBTreeNode<T>) parent;
    }

}
