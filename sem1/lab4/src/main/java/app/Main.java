package app;


import app.tree.ListTree;
import app.tree.TreeNode;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {

//        ListTree<Integer, String> tree = new ListTree<>();
//
//        tree.put(13, "data13");
//        tree.put(8, "data8");
//        tree.put(17, "data17");
//        tree.put(1, "data1");
//        tree.put(11, "data11");
//        tree.put(15, "data15");
//        tree.put(25, "data25");
//        tree.put(6, "data6");
//        tree.print();
//        tree.put(22, "data22");
//        tree.put(27, "data27");
//        tree.print();
//        tree.remove(15);
//        System.out.println("----------------------------------------------------------");
//        tree.print();

        TreeNode<Integer> node1 = new TreeNode<>(1);
        node1.addChild(new TreeNode<>(12));
        node1.addChild(new TreeNode<>(13));
        node1.addChild(new TreeNode<>(1));
        TreeNode<Integer> node2 = node1.getChild(0);
        node2.addChild(new TreeNode<>(22));
        node2.addChild(new TreeNode<>(76));
        System.out.println(TreeNode.BreadthFirstTraversal(node1));
    }
}
