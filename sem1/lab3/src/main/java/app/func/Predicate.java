package app.func;

public interface Predicate<T> {
    boolean test(T Element);
}

