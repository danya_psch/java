package app.func;

import java.util.function.Function;



public class FunctionSetUtils {
    public static <T> PurelyFunctionalSet<T> empty() {
        return x -> false;
    }

    public static <T> PurelyFunctionalSet<T> singeltonSet(T val) {
        return x -> x.equals(val);
    }

    public static <T> PurelyFunctionalSet<T> union(PurelyFunctionalSet<T> s,
                                                   PurelyFunctionalSet<T> t) {
          return x -> s.contains(x) || t.contains(x);
    }

    public static <T> PurelyFunctionalSet<T> intersect(PurelyFunctionalSet<T> s,
                                                       PurelyFunctionalSet<T> t) {
        return x -> s.contains(x) && t.contains(x);
    }

    public static <T> PurelyFunctionalSet<T> diff(PurelyFunctionalSet<T> s,
                                                  PurelyFunctionalSet<T> t) {
        return x -> s.contains(x) && !t.contains(x);
    }

    public static <T> PurelyFunctionalSet<T> filter(PurelyFunctionalSet<T> s,
                                                    Predicate<T> p) {
        return x -> s.contains(x) && p.test(x);
    }

    public static boolean forall(PurelyFunctionalSet<Integer> s,
                                     Predicate<Integer> p) {
        return iteration(s, p, 1000);
    }

    

    private static boolean iteration(PurelyFunctionalSet<Integer> s, Predicate<Integer> p, int i) {
        if (i < -1000) return true;
        if (s.contains(i) && !p.test(i)) return false;

        return iteration(s, p,i - 1);
    }

    public static <T> boolean exists(PurelyFunctionalSet<Integer> s,
                                     Predicate<Integer> p) {
        return !forall(s, x -> !p.test(x));
    }

    public static <R> PurelyFunctionalSet<R> map(PurelyFunctionalSet<Integer> s,
                                  Function<Integer, R> p) {
        return y -> exists(s, x -> y == p.apply(x));
    }
    
    //////////////////////

    public static PurelyFunctionalSet<Integer> range(int start, int end) {
        return x -> x >= start && x <= end;
    }

    public static PurelyFunctionalSet<Integer> rangeEven(int start, int end) {
        PurelyFunctionalSet<Integer> inner = range(start, end);
        return x -> x % 2 != 0 && inner.contains(x);
    }

    public static PurelyFunctionalSet<Integer> oneElement(int element) {
        return (x) -> x.equals(element);
    }

    public static PurelyFunctionalSet<Integer> addElement(int element, PurelyFunctionalSet<Integer> fs) {
        return (x) -> x.equals(element) || fs.contains(element);
    }
}