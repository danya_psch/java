package app.func;

public interface PurelyFunctionalSet<T> {
    boolean contains(T Element);
}
