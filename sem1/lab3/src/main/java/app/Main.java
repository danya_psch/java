package app;

import app.func.*;

public class Main {
    public static void main(String[] args) {

        //PurelyFunctionalSet<Integer> sss = FunctionSetUtils.range(1, 10);
        //PurelyFunctionalSet<Integer> sss2 = FunctionSetUtils.rangeEven(1, 10);
        //PurelyFunctionalSet<Integer> sss3 = FunctionSetUtils.addElement(11, sss);

        PurelyFunctionalSet<Integer> sss2 = FunctionSetUtils.singeltonSet(10);

        //System.out.println(sss3.contains(11));
        PurelyFunctionalSet<Integer> pfs1 = FunctionSetUtils.range(1, 10);

        PurelyFunctionalSet<Integer> pfs2 = FunctionSetUtils.map(pfs1, x -> x * 2);
        System.out.println(pfs2.contains(1));
        System.out.println(pfs2.contains(2));
        System.out.println(pfs2.contains(3));
        System.out.println(pfs2.contains(4));
        System.out.println(pfs2.contains(5));
        System.out.println(pfs2.contains(6));
        System.out.println(pfs2.contains(7));
        System.out.println(pfs2.contains(8));
        System.out.println(pfs2.contains(9));
        System.out.println(pfs2.contains(10));
        System.out.println(pfs2.contains(11));
        System.out.println(pfs2.contains(12));
        System.out.println(pfs2.contains(13));
        System.out.println(pfs2.contains(14));
        System.out.println(pfs2.contains(15));
        System.out.println(pfs2.contains(16));
        System.out.println(pfs2.contains(17));
        System.out.println(pfs2.contains(18));
        System.out.println(pfs2.contains(19));
        System.out.println(pfs2.contains(20));
        //System.out.println(fact(4));
    }
}

