package app.func;

import app.func.*;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class FunctionSetUtilsTest {



    @Test
    void empty() {
        PurelyFunctionalSet<Integer> pfsInt= FunctionSetUtils.empty();
        assertFalse(pfsInt.contains(-10));
        assertFalse(pfsInt.contains(0));
        assertFalse(pfsInt.contains(123));
    }

    @Test
    void singeltonSet() {
        PurelyFunctionalSet<String> pfsString = FunctionSetUtils.singeltonSet("SomeString");
        PurelyFunctionalSet<Integer> pfsInt = FunctionSetUtils.singeltonSet(1);
        assertTrue(pfsString.contains("SomeString"));
        assertFalse(pfsString.contains("SameString"));
        assertTrue(pfsInt.contains(1));
        assertFalse(pfsInt.contains(2));
    }

    @Test
    void union() {
        PurelyFunctionalSet<Integer> pfsInt1 = FunctionSetUtils.range(1, 10);
        PurelyFunctionalSet<Integer> pfsInt2 = FunctionSetUtils.range(5, 15);
        PurelyFunctionalSet<Integer> pfsInt3 = FunctionSetUtils.union(pfsInt1, pfsInt2);
        assertFalse(pfsInt3.contains(16));
        assertFalse(pfsInt3.contains(0));
        assertTrue(pfsInt3.contains(3));
        assertTrue(pfsInt3.contains(8));
        assertTrue(pfsInt3.contains(13));
    }

    @Test
    void intersect() {
        PurelyFunctionalSet<Integer> pfsInt1 = FunctionSetUtils.range(1, 10);
        PurelyFunctionalSet<Integer> pfsInt2 = FunctionSetUtils.range(5, 15);
        PurelyFunctionalSet<Integer> pfsInt3 = FunctionSetUtils.intersect(pfsInt1, pfsInt2);
        assertFalse(pfsInt3.contains(16));
        assertFalse(pfsInt3.contains(0));
        assertFalse(pfsInt3.contains(3));
        assertFalse(pfsInt3.contains(13));
        assertTrue(pfsInt3.contains(8));
    }

    @Test
    void diff() {
        PurelyFunctionalSet<Integer> pfsInt1 = FunctionSetUtils.range(1, 10);
        PurelyFunctionalSet<Integer> pfsInt2 = FunctionSetUtils.range(5, 15);
        PurelyFunctionalSet<Integer> pfsInt3 = FunctionSetUtils.diff(pfsInt1, pfsInt2);
        assertFalse(pfsInt3.contains(16));
        assertFalse(pfsInt3.contains(0));
        assertFalse(pfsInt3.contains(13));
        assertFalse(pfsInt3.contains(8));
        assertTrue(pfsInt3.contains(3));
    }

    @Test
    void filter() {
        PurelyFunctionalSet<Integer> pfsInt1 = FunctionSetUtils.range(1, 10);
        Predicate<Integer> predicate = x -> x % 2 == 0;
        PurelyFunctionalSet<Integer> pfsInt2 = FunctionSetUtils.filter(pfsInt1, predicate);

        assertFalse(pfsInt2.contains(1));
        assertFalse(pfsInt2.contains(3));
        assertFalse(pfsInt2.contains(5));
        assertFalse(pfsInt2.contains(7));
        assertFalse(pfsInt2.contains(9));

        assertTrue(pfsInt2.contains(2));
        assertTrue(pfsInt2.contains(4));
        assertTrue(pfsInt2.contains(6));
        assertTrue(pfsInt2.contains(8));
        assertTrue(pfsInt2.contains(10));
    }

    @Test
    void forall() {
        PurelyFunctionalSet<Integer> pfsInt = FunctionSetUtils.range(1, 100);

        assertFalse(FunctionSetUtils.forall(pfsInt, x -> x < 0));
        assertFalse(FunctionSetUtils.forall(pfsInt, x -> x % 5 <= 1));
        assertTrue(FunctionSetUtils.forall(pfsInt, x -> x > 0));
        assertTrue(FunctionSetUtils.forall(pfsInt, x -> x <= 100));
    }

    @Test
    void exists() {
        PurelyFunctionalSet<Integer> pfsInt = FunctionSetUtils.range(1, 10);

        assertFalse(FunctionSetUtils.exists(pfsInt, x -> x < 0));
        assertFalse(FunctionSetUtils.exists(pfsInt, x -> x > 10));
        assertTrue(FunctionSetUtils.exists(pfsInt, x -> x > 9));
        assertTrue(FunctionSetUtils.exists(pfsInt, x -> x % 5 <= 1));
    }

    @Test
    void map() {
        PurelyFunctionalSet<Integer> pfsInt1 = FunctionSetUtils.range(1, 10);
        PurelyFunctionalSet<Integer> pfsInt2 = FunctionSetUtils.map(pfsInt1, x -> x * 2);

        assertFalse(pfsInt2.contains(1));
        assertFalse(pfsInt2.contains(3));
        assertFalse(pfsInt2.contains(19));
        assertFalse(pfsInt2.contains(21));

        assertTrue(pfsInt2.contains(2));
        assertTrue(pfsInt2.contains(4));
        assertTrue(pfsInt2.contains(20));
    }


}