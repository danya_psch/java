package app.dao;

import app.model.*;

import java.util.List;

public interface IDAO {
    Programmer getProgrammer(Long Id);
    List<Programmer> getProgrammerList();

    PCGame getPCGame(Long Id);
    List<PCGame> getPCGameList();

    ConsoleGame getConsoleGame(Long Id);
    List<ConsoleGame> getConsoleGameList();

    Player getPlayer(Long Id);
    List<Player> getPlayerList();

    DeveloperCompany getDeveloperCompany(Long Id);
    List<DeveloperCompany> getDeveloperCompanyList();
}