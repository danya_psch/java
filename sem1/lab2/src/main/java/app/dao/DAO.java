package app.dao;

import app.model.*;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;
import java.util.Properties;

public class DAO implements IDAO {

    IDAOImpl<Programmer> programmerIDAOImpl;
    IDAOImpl<PCGame> pcGameIDAOImpl;
    IDAOImpl<ConsoleGame> consoleGameIDAOImpl;
    IDAOImpl<Player> playerIDAOImpl;
    IDAOImpl<DeveloperCompany> developerCompanyIDAOImpl;

    public DAO() throws SQLException  {
        String url = "jdbc:postgresql://localhost:5432/postgres";
        Properties props = new Properties();
        props.setProperty("user","postgres");
        props.setProperty("password","123");
        Connection connection = DriverManager.getConnection(url, props);

        programmerIDAOImpl = new DAOImpl<>(Programmer.class, connection);
        pcGameIDAOImpl = new DAOImpl<>(PCGame.class, connection);
        consoleGameIDAOImpl = new DAOImpl<>(ConsoleGame.class, connection);
        playerIDAOImpl = new DAOImpl<>(Player.class, connection);
        developerCompanyIDAOImpl = new DAOImpl<>(DeveloperCompany.class, connection);
    }

    @Override
    public Programmer getProgrammer(Long Id) {
        return programmerIDAOImpl.getEntity(Id);
    }
    @Override
    public List<Programmer> getProgrammerList() {
        return programmerIDAOImpl.getEntityList();
    }

    @Override
    public PCGame getPCGame(Long Id) {
        return pcGameIDAOImpl.getEntity(Id);
    }
    @Override
    public List<PCGame> getPCGameList() {
        return pcGameIDAOImpl.getEntityList();
    }

    @Override
    public ConsoleGame getConsoleGame(Long Id) {
        return consoleGameIDAOImpl.getEntity(Id);
    }
    @Override
    public List<ConsoleGame> getConsoleGameList() {
        return consoleGameIDAOImpl.getEntityList();
    }

    @Override
    public Player getPlayer(Long Id) {
        return playerIDAOImpl.getEntity(Id);
    }
    @Override
    public List<Player> getPlayerList() {
        return playerIDAOImpl.getEntityList();
    }

    @Override
    public DeveloperCompany getDeveloperCompany(Long Id) {
        return developerCompanyIDAOImpl.getEntity(Id);
    }
    @Override
    public List<DeveloperCompany> getDeveloperCompanyList() {
        return developerCompanyIDAOImpl.getEntityList();
    }


}