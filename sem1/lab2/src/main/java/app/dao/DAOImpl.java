package app.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.apache.commons.lang3.ArrayUtils;

import java.lang.reflect.Field;

import app.annotation.*;

public class DAOImpl<T> implements IDAOImpl<T> {

    private Class<T> clazz;
    private Connection connection;

    public DAOImpl(Class<T> clazz) throws SQLException {
        this.clazz = clazz;
        String url = "jdbc:postgresql://localhost:5432/postgres";
        Properties props = new Properties();
        props.setProperty("user","postgres");
        props.setProperty("password","123");
        this.connection = DriverManager.getConnection(url, props);
    }

    public DAOImpl(Class<T> clazz, Connection connection) {
        this.clazz = clazz;
        this.connection = connection;
    }

    @Override
    public T getEntity(Long id) {
        try {
            if (isEntity(clazz)) {
                ResultSet resultSet = connection.createStatement().executeQuery(createQueryString(clazz, id));
                if (!resultSet.next()) return null;
                return createNewInstance(resultSet);
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public List<T> getEntityList() {
        try {
            if (isEntity(clazz)) {
                ResultSet resultSet = connection.createStatement().executeQuery(createQueryString(clazz, null));
                List<T> list = new ArrayList<T>();
                while (resultSet.next()) {
                    list.add(createNewInstance(resultSet));
                }

                return list;
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    private static boolean isEntity(Class<?> clazz) {
        return clazz.isAnnotationPresent(Entity.class);
    }

    private String createQueryString(Class<T> clazz, Long id) {
        String query = "SELECT * FROM public." + clazz.getAnnotation(Entity.class).table();
        boolean idIsNull = true;

        if (id != null) {
            idIsNull = false;
            query += " WHERE id = " + id;
        }

        Class<? super T> superClazz = clazz.getSuperclass();
        if (superClazz.isAnnotationPresent(DiscriminatorColumn.class) &&
                clazz.isAnnotationPresent(DiscriminatorValue.class))
        {
            if (!idIsNull) {
                query += " AND ";
            } else {
                query += " WHERE ";
            }
            query += superClazz.getAnnotation(DiscriminatorColumn.class).name() +
                    " = \'" + clazz.getAnnotation(DiscriminatorValue.class).type() + "\'";
        }
        return query;
    }

    private T createNewInstance(ResultSet resultSet) throws Exception {
        Field[] fields = clazz.getDeclaredFields();
        Class<? super T> superClazz = clazz.getSuperclass();
        if (superClazz.isAnnotationPresent(DiscriminatorColumn.class)) {
            fields = ArrayUtils.addAll(fields, superClazz.getDeclaredFields());
        }

        T obj = clazz.getDeclaredConstructor().newInstance();
        for (Field field : fields) {
            field.setAccessible(true);
            if (!field.isAnnotationPresent(Column.class)) continue;
            switch (field.getType().toString()) {
                case ("class java.lang.Long") :
                    field.set(obj, (Long)resultSet.getLong(field.getAnnotation(Column.class).name()));
                    break;
                case ("class java.lang.String") :
                    field.set(obj, (String)resultSet.getString(field.getAnnotation(Column.class).name()));
                    break;
                case ("class java.lang.Integer") :
                    field.set(obj, (Integer)resultSet.getInt(field.getAnnotation(Column.class).name()));
                    break;
                case ("class java.time.LocalDate") :
                    field.set(obj, resultSet.getDate(field.getAnnotation(Column.class).name()).toLocalDate());
                    break;
            }
        }
        return obj;
    }


}