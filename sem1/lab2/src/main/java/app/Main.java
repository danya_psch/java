package app;

import java.util.List;

import app.model.*;

import app.dao.DAO;



public class Main {
    public static void main(String[] args) {
        try {
            DAO dao = new DAO();
            List<PCGame> pcGames = dao.getPCGameList();
            Game consoleGame = dao.getConsoleGame(2L);
            Programmer programmer = dao.getProgrammer(1L);
            List<DeveloperCompany> developer_companies = dao.getDeveloperCompanyList();

            for (PCGame pcGame: pcGames) {
                System.out.println(pcGame.toString());
            }
            System.out.println(consoleGame.toString() + programmer.toString());

            for (DeveloperCompany developer_company: developer_companies) {
                System.out.println(developer_company.toString());
            }
        }
        catch(Exception e) {
            e.printStackTrace();
        }
    }
}
