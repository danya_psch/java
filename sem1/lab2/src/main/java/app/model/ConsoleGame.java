package app.model;

import app.annotation.Column;
import app.annotation.Entity;
import app.annotation.DiscriminatorValue;

import java.time.LocalDate;

@Entity(table = "game")
@DiscriminatorValue(type = "console_game")
public class ConsoleGame extends Game {
    @Column(name = "console_type")
    private String consoleType;

    public ConsoleGame() {}
    public ConsoleGame(Long id, String name, String genre, Integer ageLimit, LocalDate releaseDate, String consoleType) {
        super(id, name, genre, ageLimit, releaseDate);
        this.consoleType = consoleType;
    }

    public String getConsoleType() {
        return consoleType;
    }
    public void setConsoleType(String consoleType) {
        this.consoleType = consoleType;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;
        ConsoleGame consoleGame = (ConsoleGame) obj;
        return id.equals(consoleGame.id) &&
                name.equals(consoleGame.name) &&
                genre.equals(consoleGame.genre) &&
                ageLimit.equals(consoleGame.ageLimit) &&
                releaseDate.equals(consoleGame.releaseDate) &&
                consoleType.equals(consoleGame.consoleType);
    }

    @Override
    public String toString() {
        return "ConsoleGame : " +
                "id = " + id +
                ", name = " + name +
                ", genre = " + genre +
                ", age_limit = " + ageLimit +
                ", release_date = " + releaseDate +
                ", console_type = " + consoleType + "\n";
    }

}
