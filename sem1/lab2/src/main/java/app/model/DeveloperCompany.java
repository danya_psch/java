package app.model;

import app.annotation.*;

import java.time.LocalDate;

@Entity(table = "developer_company")
public class DeveloperCompany {
    @Primary
    @Column(name = "id")
    private Long id;
    @Column(name = "name")
    private String name;
    @Column(name = "employees_count")
    private Integer employeesCount;
    @Column(name = "website")
    private String website;
    @Column(name = "date_of_establishment")
    private LocalDate dateOfEstablishment;

    public DeveloperCompany() {}
    public DeveloperCompany(Long id, String name, Integer employeesCount, String website, LocalDate dateOfEstablishment) {
        this.id = id;
        this.name = name;
        this.employeesCount = employeesCount;
        this.website = website;
        this.dateOfEstablishment = dateOfEstablishment;
    }

    public Long getId() { return id; }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public Integer getEmployeesCount() {
        return employeesCount;
    }
    public void setEmployeesCount(Integer employeesCount) {
        this.employeesCount = employeesCount;
    }

    public String getWebsite() {
        return website;
    }
    public void setWebsite(String website) {
        this.website = website;
    }

    public LocalDate getDateOfEstablishment() {
        return dateOfEstablishment;
    }
    public void setDateOfEstablishment(LocalDate dateOfEstablishment) {
        this.dateOfEstablishment = dateOfEstablishment;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;
        DeveloperCompany developerCompany = (DeveloperCompany) obj;
        return id.equals(developerCompany.id) &&
                name.equals(developerCompany.name) &&
                employeesCount.equals(developerCompany.employeesCount) &&
                website.equals(developerCompany.website) &&
                dateOfEstablishment.equals(developerCompany.dateOfEstablishment);
    }

    @Override
    public String toString() {
        return "DeveloperCompany : " +
                "id = " + id +
                ", name = " + name +
                ", employees_count = " + employeesCount +
                ", website = " + website +
                ", date_of_establishment = " + dateOfEstablishment + "\n";
    }
}
