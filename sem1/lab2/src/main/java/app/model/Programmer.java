package app.model;

import java.time.LocalDate;

import app.annotation.*;


@Entity(table = "programmer")
public class Programmer {
    @Primary
    @Column(name = "id")
    private Long id;
    @Column(name = "name")
    private String name;
    @Column(name = "salary")
    private Integer salary;
    @Column(name = "date_of_birth")
    private LocalDate dateOfBirth;

    public Programmer() {}

    public Programmer(Long id, String name, Integer salary, LocalDate dateOfBirth) {
        this.id = id;
        this.name = name;
        this.salary = salary;
        this.dateOfBirth = dateOfBirth;
    }

    public Long getId() { return id; }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public Integer getSalary() { return salary; }
    public void setSalary(Integer salary) { this.salary = salary; }

    public LocalDate getDateOfBirth() { return dateOfBirth; }
    public void setDateOfBirth(LocalDate dateOfBirth) { this.dateOfBirth = dateOfBirth; }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;
        Programmer programmer = (Programmer) obj;
        return id.equals(programmer.id) &&
                name.equals(programmer.name) &&
                salary.equals(programmer.salary) &&
                dateOfBirth.equals(programmer.dateOfBirth);
    }

    @Override
    public String toString() {
        return "Programmer : " +
                "id = " + id +
                ", name = " + name +
                ", salary = " + salary +
                ", date_of_birth = " + dateOfBirth + "\n";
    }
}
