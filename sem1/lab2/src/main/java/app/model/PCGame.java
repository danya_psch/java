package app.model;

import app.annotation.Column;
import app.annotation.DiscriminatorValue;
import app.annotation.Entity;

import java.time.LocalDate;

@Entity(table = "game")
@DiscriminatorValue(type = "pc_game")
public class PCGame extends Game {
    @Column(name = "system_requirements")
    private String systemRequirements;

    public PCGame() {}
    public PCGame(Long id, String name, String genre, Integer ageLimit, LocalDate releaseDate, String systemRequirements) {
        super(id, name, genre, ageLimit, releaseDate);
        this.systemRequirements = systemRequirements;
    }

    public String getSystemRequirements() {
        return systemRequirements;
    }
    public void setSystemRequirements(String systemRequirements) {
        this.systemRequirements = systemRequirements;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;
        PCGame pcGame = (PCGame) obj;
        return id.equals(pcGame.id) &&
                name.equals(pcGame.name) &&
                genre.equals(pcGame.genre) &&
                ageLimit.equals(pcGame.ageLimit) &&
                releaseDate.equals(pcGame.releaseDate) &&
                systemRequirements.equals(pcGame.systemRequirements);
    }

    @Override
    public String toString() {
        return "PCGame : " +
                "id = " + id +
                ", name = " + name +
                ", genre = " + genre +
                ", age_limit = " + ageLimit +
                ", release_date = " + releaseDate +
                ", system_requirements = " + systemRequirements + "\n";
    }

}
