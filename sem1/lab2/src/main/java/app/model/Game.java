package app.model;

import java.time.LocalDate;
import java.util.Date;

import app.annotation.*;

@Entity(table = "game")
@DiscriminatorColumn(name = "game_type")
public abstract class Game {
    @Primary
    @Column(name = "id")
    protected Long id;
    @Column(name = "name")
    protected String name;
    @Column(name = "genre")
    protected String genre;
    @Column(name = "age_limit")
    protected Integer ageLimit;
    @Column(name = "release_date")
    protected LocalDate releaseDate;

    public Game() {}
    public Game(Long id, String name, String genre, Integer ageLimit, LocalDate releaseDate) {
        this.id = id;
        this.name = name;
        this.genre = genre;
        this.ageLimit = ageLimit;
        this.releaseDate = releaseDate;
    }

    public Long getId() { return id; }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public String getGenre() {
        return genre;
    }
    public void setGenre(String genre) {
        this.genre = genre;
    }

    public Integer getAgeLimit() {
        return ageLimit;
    }
    public void setAgeLimit(Integer ageLimit) {
        this.ageLimit = ageLimit;
    }

    public LocalDate getReleaseDate() {
        return releaseDate;
    }
    public void setReleaseDate(LocalDate releaseDate) {
        this.releaseDate = releaseDate;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;
        Game game = (Game) obj;
        return id.equals(game.id) &&
                name.equals(game.name) &&
                genre.equals(game.genre) &&
                ageLimit.equals(game.ageLimit) &&
                releaseDate.equals(game.releaseDate);
    }

    @Override
    public String toString() {
        return "Game : " +
                "id = " + id +
                ", name = " + name +
                ", genre = " + genre +
                ", age_limit = " + ageLimit +
                ", release_date = " + releaseDate + "\n";
    }
}
