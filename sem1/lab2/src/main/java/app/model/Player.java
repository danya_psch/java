package app.model;

import app.annotation.*;

@Entity(table = "player")
public class Player {
    @Primary
    @Column(name = "id")
    private Long id;
    @Column(name = "name")
    private String name;
    @Column(name = "ip")
    private String ip;
    @Column(name = "age")
    private Integer age;

    public Player() {}
    public Player(Long id, String name, String ip, Integer age) {
        this.id = id;
        this.name = name;
        this.ip = ip;
        this.age = age;
    }

    public Long getId() { return id; }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public String getIp() { return ip; }
    public void setSalary(String ip) { this.ip = ip; }

    public Integer getAge() { return age; }
    public void setDateOfBirth(Integer age) { this.age = age; }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;
        Player player = (Player)obj;
        return id.equals(player.id) &&
                name.equals(player.name) &&
                ip.equals(player.ip) &&
                age.equals(player.age);
    }

    @Override
    public String toString() {
        return "Player : " +
                "id = " + id +
                ", name = " + name +
                ", ip = " + ip +
                ", age = " + age + "\n";
    }
}
