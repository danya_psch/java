package app.dao;

import app.model.Player;

import java.util.List;
import java.util.ArrayList;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import java.sql.SQLException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.Mockito.when;

@DisplayName("Player")
@ExtendWith(MockitoExtension.class)
public class DAOImplPlayerTest extends DAOImplTest {
    @Test
    @DisplayName("getEntityById")
    public void getEntity() throws SQLException {
        DAOImpl<Player> playerDao = new DAOImpl<>(Player.class, mockConnection);
        when(mockResultSet.next()).thenReturn(true, false);
        when(mockResultSet.getLong("id")).thenReturn(1L);
        when(mockResultSet.getString("name")).thenReturn("Vlad");
        when(mockResultSet.getString("ip")).thenReturn("83.111.123.246");
        when(mockResultSet.getInt("age")).thenReturn(12);
        Player playerId1 = new Player(1L, "Vlad", "83.111.123.246", 12);
        assertEquals(playerId1, playerDao.getEntity(1L));
        assertNull(playerDao.getEntity(8L));

    }

    @Test
    @DisplayName("getEntityList")
    public void getEntityList() throws SQLException {
        DAOImpl<Player> playerDao = new DAOImpl<>(Player.class, mockConnection);
        when(mockResultSet.next()).thenReturn(false, true, true, false);
        assertEquals(playerDao.getEntityList().size(), 0);
        when(mockResultSet.getLong("id")).thenReturn(1L, 2L);
        when(mockResultSet.getString("name")).thenReturn("Vlad", "Danya");
        when(mockResultSet.getString("ip")).thenReturn("83.111.123.246", "83.111.123.247");
        when(mockResultSet.getInt("age")).thenReturn(12, 10);
        List<Player> list = playerDao.getEntityList();
        List<Player> expectedList = new ArrayList<Player>();
        expectedList.add(new Player(1L, "Vlad", "83.111.123.246", 12));
        expectedList.add(new Player(2L, "Danya", "83.111.123.247", 10)); //redo
        assertEquals(list.size(), expectedList.size());
        for (int i = 0; i < list.size(); i++) {
            assertEquals(list.get(i), expectedList.get(i));
        }
    }
}