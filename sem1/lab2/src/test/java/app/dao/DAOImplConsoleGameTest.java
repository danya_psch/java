package app.dao;

import app.model.ConsoleGame;

import java.util.List;
import java.util.ArrayList;

import java.time.LocalDate;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import java.sql.SQLException;
import java.sql.Date;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.Mockito.when;

@DisplayName("ConsoleGame")
@ExtendWith(MockitoExtension.class)
class DAOImplConsoleGameTest extends DAOImplTest{

    @Test
    @DisplayName("getEntityById")
    public void getEntity() throws SQLException {

        DAOImpl<ConsoleGame> consoleGameDao = new DAOImpl<>(ConsoleGame.class, mockConnection);
        when(mockResultSet.next()).thenReturn(true, false);
        when(mockResultSet.getLong("id")).thenReturn(2L);
        when(mockResultSet.getString("name")).thenReturn("Dark Souls III");
        when(mockResultSet.getString("genre")).thenReturn("Action role-playing");
        when(mockResultSet.getInt("age_limit")).thenReturn(16);
        when(mockResultSet.getDate("release_date")).thenReturn(Date.valueOf(LocalDate.parse("2016-04-12")));
        when(mockResultSet.getString("console_type")).thenReturn("Xbox One");
        ConsoleGame consoleGameId2 = new ConsoleGame(2L, "Dark Souls III", "Action role-playing", 16, LocalDate.parse("2016-04-12"), "Xbox One");
        assertEquals(consoleGameId2, consoleGameDao.getEntity(2L));
        assertNull(consoleGameDao.getEntity(8L));
    }

    @Test
    @DisplayName("getEntityList")
    public void getEntityList() throws SQLException {

        DAOImpl<ConsoleGame> consoleGameDao = new DAOImpl<>(ConsoleGame.class, mockConnection);
        when(mockResultSet.next()).thenReturn(false, true, true, false);
        assertEquals(consoleGameDao.getEntityList().size(), 0);
        when(mockResultSet.getLong("id")).thenReturn(2L, 4L);
        when(mockResultSet.getString("name")).thenReturn("Dark Souls III", "Bloodborne");
        when(mockResultSet.getString("genre")).thenReturn("Action role-playing", "Action role-playing");
        when(mockResultSet.getInt("age_limit")).thenReturn(16, 16);
        when(mockResultSet.getDate("release_date")).thenReturn(
                Date.valueOf(LocalDate.parse("2015-03-19")),
                Date.valueOf(LocalDate.parse("2015-03-24"))
        );
        when(mockResultSet.getString("console_type")).thenReturn("Xbox One", "PlayStation 4");
        List<ConsoleGame> list = consoleGameDao.getEntityList();
        List<ConsoleGame> expectedList = new ArrayList<>();
        expectedList.add(new ConsoleGame(2L,"Dark Souls III", "Action role-playing", 16, LocalDate.parse("2015-03-19"), "Xbox One"));
        expectedList.add(new ConsoleGame(4L, "Bloodborne", "Action role-playing", 16, LocalDate.parse("2015-03-24"), "PlayStation 4"));
        assertEquals(list.size(), expectedList.size());
        for (int i = 0; i < list.size(); i++) {
            assertEquals(list.get(i), expectedList.get(i));
        }
    }
}