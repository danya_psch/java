package app.dao;

import app.model.PCGame;

import java.util.List;
import java.util.ArrayList;

import java.time.LocalDate;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import java.sql.SQLException;
import java.sql.Date;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.Mockito.when;

@DisplayName("PCGame")
@ExtendWith(MockitoExtension.class)
class DAOImplPCGameTest extends DAOImplTest{

    @Test
    @DisplayName("getEntityById")
    public void getEntity() throws SQLException {

        DAOImpl<PCGame> pcGameDao = new DAOImpl<>(PCGame.class, mockConnection);
        when(mockResultSet.next()).thenReturn(true, false);
        when(mockResultSet.getLong("id")).thenReturn(1L);
        when(mockResultSet.getString("name")).thenReturn("Witcher 3");
        when(mockResultSet.getString("genre")).thenReturn("Action role-playing");
        when(mockResultSet.getInt("age_limit")).thenReturn(14);
        when(mockResultSet.getDate("release_date")).thenReturn(Date.valueOf(LocalDate.parse("2015-03-19")));
        when(mockResultSet.getString("system_requirements")).thenReturn("Core i5 2500K...");
        PCGame pcGameId1 = new PCGame(1L, "Witcher 3", "Action role-playing", 14, LocalDate.parse("2015-03-19"), "Core i5 2500K...");
        assertEquals(pcGameId1, pcGameDao.getEntity(1L));
        assertNull(pcGameDao.getEntity(8L));
    }

    @Test
    @DisplayName("getEntityList")
    public void getEntityList() throws SQLException {

        DAOImpl<PCGame> pcGameDao = new DAOImpl<>(PCGame.class, mockConnection);
        when(mockResultSet.next()).thenReturn(false, true, true, false);
        assertEquals(pcGameDao.getEntityList().size(), 0);
        when(mockResultSet.getLong("id")).thenReturn(1L, 3L);
        when(mockResultSet.getString("name")).thenReturn("Witcher 3", "Metro Exodus");
        when(mockResultSet.getString("genre")).thenReturn("Action role-playing", "First-person shooter");
        when(mockResultSet.getInt("age_limit")).thenReturn(14, 16);
        when(mockResultSet.getDate("release_date")).thenReturn(
                Date.valueOf(LocalDate.parse("2015-03-19")),
                Date.valueOf(LocalDate.parse("2019-02-15"))
        );
        when(mockResultSet.getString("system_requirements")).thenReturn("Core i5 2500K...", "Core i7-8700K...");
        List<PCGame> list = pcGameDao.getEntityList();
        List<PCGame> expectedList = new ArrayList<PCGame>();
        expectedList.add(new PCGame(1L,"Witcher 3", "Action role-playing", 14, LocalDate.parse("2015-03-19"), "Core i5 2500K..."));
        expectedList.add(new PCGame(3L, "Metro Exodus", "First-person shooter", 16, LocalDate.parse("2019-02-15"), "Core i7-8700K..."));
        assertEquals(list.size(), expectedList.size());
        for (int i = 0; i < list.size(); i++) {
            assertEquals(list.get(i), expectedList.get(i));
        }
    }
}