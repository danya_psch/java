package app.dao;

import app.model.DeveloperCompany;
import app.dao.DAOImpl;

import java.util.List;
import java.util.ArrayList;
import java.sql.Date;

import java.time.LocalDate;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import java.sql.SQLException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.Mockito.when;

@DisplayName("DeveloperCompany")
@ExtendWith(MockitoExtension.class)
public class DAOImplDeveloperCompanyTest extends DAOImplTest {

    @Test
    @DisplayName("getEntityById")
    public void getEntity() throws SQLException {
        DAOImpl<DeveloperCompany> developerCompanyDao = new DAOImpl<>(DeveloperCompany.class, mockConnection);
        when(mockResultSet.next()).thenReturn(true, false);
        when(mockResultSet.getLong("id")).thenReturn(1L);
        when(mockResultSet.getString("name")).thenReturn("FromSoftware");
        when(mockResultSet.getInt("employees_count")).thenReturn(283);
        when(mockResultSet.getString("website")).thenReturn("www.fromsoftware.jp/ww/");
        when(mockResultSet.getDate("date_of_establishment")).thenReturn(Date.valueOf(LocalDate.parse("1986-11-01")));
        DeveloperCompany developerCompanyId1 = new DeveloperCompany(1L, "FromSoftware", 283, "www.fromsoftware.jp/ww/", LocalDate.parse("1986-11-01"));
        assertEquals(developerCompanyId1, developerCompanyDao.getEntity(1L));
        assertNull(developerCompanyDao.getEntity(8L));
    }

    @Test
    @DisplayName("getEntityList")
    public void getEntityList() throws SQLException {
        DAOImpl<DeveloperCompany> developerCompanyDao = new DAOImpl<>(DeveloperCompany.class, mockConnection);
        when(mockResultSet.next()).thenReturn(false, true, true, false);
        assertEquals(developerCompanyDao.getEntityList().size(), 0);
        when(mockResultSet.getLong("id")).thenReturn(1L, 2L);
        when(mockResultSet.getString("name")).thenReturn("FromSoftware", "CD Projekt");
        when(mockResultSet.getInt("employees_count")).thenReturn(283, 887);
        when(mockResultSet.getString("website")).thenReturn("www.fromsoftware.jp/ww/", "cdprojekt.com");
        when(mockResultSet.getDate("date_of_establishment")).thenReturn(
                Date.valueOf(LocalDate.parse("1986-11-01")),
                Date.valueOf(LocalDate.parse("1994-05-01"))
        );
        List<DeveloperCompany> list = developerCompanyDao.getEntityList();
        List<DeveloperCompany> expectedList = new ArrayList<DeveloperCompany>();
        expectedList.add(new DeveloperCompany(
                1L, "FromSoftware", 283, "www.fromsoftware.jp/ww/", LocalDate.parse("1986-11-01")
        ));
        expectedList.add(new DeveloperCompany(
                2L, "CD Projekt", 887, "cdprojekt.com", LocalDate.parse("1994-05-01")
        ));
        assertEquals(list.size(), expectedList.size());
        for (int i = 0; i < list.size(); i++) {
            assertEquals(list.get(i), expectedList.get(i));
        }
    }
}