package app.dao;

import app.model.Programmer;

import java.util.List;
import java.util.ArrayList;

import java.sql.Date;

import java.time.LocalDate;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import java.sql.SQLException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.Mockito.when;

@DisplayName("Programmer")
@ExtendWith(MockitoExtension.class)
public class DAOImplProgrammerTest extends DAOImplTest {
    @Test
    @DisplayName("getEntityById")
    public void getEntity() throws SQLException {
        DAOImpl<Programmer> programmerDao = new DAOImpl<>(Programmer.class, mockConnection);
        when(mockResultSet.next()).thenReturn(true, false);
        when(mockResultSet.getLong("id")).thenReturn(1L);
        when(mockResultSet.getString("name")).thenReturn("Vlad");
        when(mockResultSet.getInt("salary")).thenReturn(600);
        when(mockResultSet.getDate("date_of_birth")).thenReturn(Date.valueOf(LocalDate.parse("1998-05-30")));
        Programmer programmerId1 = new Programmer(1L, "Vlad", 600, LocalDate.parse("1998-05-30"));
        assertEquals(programmerId1, programmerDao.getEntity(1L));
        assertNull(programmerDao.getEntity(8L));
    }

    @Test
    @DisplayName("getEntityList")
    public void getEntityList() throws SQLException {

        DAOImpl<Programmer> programmerDao = new DAOImpl<>(Programmer.class, mockConnection);
        when(mockResultSet.next()).thenReturn(false, true, true, false);
        assertEquals(programmerDao.getEntityList().size(), 0);
        when(mockResultSet.getLong("id")).thenReturn(1L, 2L);
        when(mockResultSet.getString("name")).thenReturn("Vlad", "Danya");
        when(mockResultSet.getInt("salary")).thenReturn(600, 600);
        when(mockResultSet.getDate("date_of_birth")).thenReturn(
                Date.valueOf(LocalDate.parse("1998-05-30")),
                Date.valueOf(LocalDate.parse("2000-08-31"))
        );
        List<Programmer> list = programmerDao.getEntityList();
        List<Programmer> expectedList = new ArrayList<Programmer>();
        expectedList.add(new Programmer(1L, "Vlad", 600, LocalDate.parse("1998-05-30")));
        expectedList.add(new Programmer(2L, "Danya", 600, LocalDate.parse("2000-08-31"))); //redo
        assertEquals(list.size(), expectedList.size());
        for (int i = 0; i < list.size(); i++) {
            assertEquals(list.get(i), expectedList.get(i));
        }
    }
}