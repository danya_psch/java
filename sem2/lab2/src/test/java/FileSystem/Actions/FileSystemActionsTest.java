package FileSystem.Actions;

import FileSystem.Controller;
import FileSystem.Entities.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

@DisplayName("File System Actions")
public class FileSystemActionsTest {
    private Controller controller;

    @BeforeEach
    private void createController() {
        controller = new Controller();
    }

    @Test
    @DisplayName("LogFile Actions")
    void LogFileActionTest() {
        try {
            var logFile = controller.submitFSAction(new CreateLogTextFile(controller.getRoot(), "log", "line1")).get();
            var child = controller.submitFSAction(new GetChild(controller.getRoot(), "log")).get();
            assertEquals(logFile, child);
            controller.submitFSAction(new AppendLineToLog(logFile, "line2")).get();
            var data = controller.submitFSAction(new ReadLogFile(logFile)).get();
            assertEquals(data, "line1line2");
        }
        catch (Exception e) {
            fail(e.getMessage());
        }
    }

    @Test
    @DisplayName("BufferFile actions")
    void BufferFileActionTest() {
        try {
            var bufFile = controller.submitFSAction(new CreateBufferFile<String>(controller.getRoot(), "buf")).get();
            var child = controller.submitFSAction(new GetChild(controller.getRoot(), "buf")).get();
            assertEquals(bufFile, child);
            controller.submitFSAction(new PushToBuffer<>(bufFile, "data1")).get();
            var data1 = controller.submitFSAction(new ConsumeFromBuffer<>(bufFile)).get();
            controller.submitFSAction(new PushToBuffer<>(bufFile, "data2")).get();
            var data2 = controller.submitFSAction(new ConsumeFromBuffer<>(bufFile)).get();
            assertEquals(data1, "data1");
            assertEquals(data2, "data2");
        }
        catch (Exception e) {
            fail(e.getMessage());
        }
    }

    @Test
    @DisplayName("BinaryFile actions")
    void BinaryFileActionTest() {
        try {
            var binFile = controller.submitFSAction(new CreateBinaryFile(controller.getRoot(), "bin", "binData".getBytes())).get();
            var child = controller.submitFSAction(new GetChild(controller.getRoot(), "bin")).get();
            assertEquals(binFile, child);
            var data = controller.submitFSAction(new ReadBinaryFile(binFile)).get();
            assertArrayEquals(data, "binData".getBytes());
        }
        catch (Exception e) {
            fail(e.getMessage());
        }
    }

    @Test
    @DisplayName("NamedPipe actions")
    void NamedPipeActionTest() {
        try {
            var pipeFile = controller.submitFSAction(new CreateNamedPipe(controller.getRoot(), "pipe")).get();
            var child = controller.submitFSAction(new GetChild(controller.getRoot(), "pipe")).get();
            assertEquals(pipeFile, child);
            controller.submitFSAction(new WriteToPipe(pipeFile, "data")).get();
            var data = controller.submitFSAction(new ReadFromPipe(pipeFile)).get();
            assertEquals(data, "data");
        }
        catch (Exception e) {
            fail(e.getMessage());
        }
    }

    @Test
    @DisplayName("Entity actions")
    void EntityActionTest() {
        try {
            var binFile = controller.submitFSAction(new CreateBinaryFile(controller.getRoot(), "bin", "binData".getBytes())).get();
            controller.submitFSAction(new SetEntityName(binFile, "new_bin")).get();
            var child = controller.submitFSAction(new GetChild(controller.getRoot(), "new_bin")).get();
            assertEquals(binFile, child);
            var homeDir = controller.submitFSAction(new CreateDirectory(controller.getRoot(), "home")).get();
            var imgDir = controller.submitFSAction(new CreateDirectory(homeDir, "img")).get();
            controller.submitFSAction(new MoveEntity(homeDir, binFile)).get();
            child = controller.submitFSAction(new GetChild(homeDir, "new_bin")).get();
            assertEquals(binFile, child);
            controller.submitFSAction(new MoveEntity("/home/img", binFile)).get();
            child = controller.submitFSAction(new GetChild(imgDir, "new_bin")).get();
            assertEquals(binFile, child);
            assertEquals(controller.submitFSAction(new GetEntityName(binFile)).get(), "new_bin");
            assertEquals(controller.submitFSAction(new GetEntityType(binFile)).get(), Entity.EntityType.BINARY_FILE);

        }
        catch (Exception e) {
            fail(e.getMessage());
        }
    }

    @Test
    @DisplayName("Delete action")
    void DeleteActionTest() {
        try {
            var homeDir = controller.submitFSAction(new CreateDirectory(controller.getRoot(), "home")).get();
            var binFile = controller.submitFSAction(new CreateBinaryFile(homeDir, "bin", "binData".getBytes())).get();
            var res = controller.submitFSAction(new DeleteEntity(binFile)).get();
            assertEquals(controller.submitFSAction(new GetChildren(homeDir)).get().size(), 0);
        }
        catch (Exception e) {
            fail(e.getMessage());
        }
    }
}
