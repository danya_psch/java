package FileSystem.Entities;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertThrows;

@DisplayName("Named pipe")
public class NamedPipeTest {
    @Test
    @DisplayName("Create pipe")
    void createFileTest() {
        assertThrows(IllegalArgumentException.class, () -> NamedPipe.create("test", null));

        var dir = Directory.create("some", null);
        assertThrows(IllegalArgumentException.class, () -> NamedPipe.create("", dir));

        var file = NamedPipe.create("file sample", dir);
        assertNotNull(file);
        assertEquals(Entity.EntityType.NAMED_PIPE, file.getType());
        assertEquals("file sample", file.getName());
        assertFalse(file.isDirectory());
    }

    @Test
    @DisplayName("Read write")
    void readWriteTest() {
        var dir = Directory.create("dir", null);
        var file = NamedPipe.create("file", dir);

        file.write("some data ");
        assertEquals("some data ", file.read());
        assertThrows(IllegalArgumentException.class, () -> file.write(null));
    }
}
