package FileSystem;


import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;

import static org.junit.jupiter.api.Assertions.*;

@DisplayName("Stress test")
public class StressTest {
    @Test
    @DisplayName("Perform stress test")
    void stressTest() {
        var controller = new Controller();
        var generator = new Generator(controller);
        assertDoesNotThrow(() -> generator.performFSStressTest(1000));
    }
}