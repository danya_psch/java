package FileSystem;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import FileSystem.Entities.Directory;
import FileSystem.Entities.NamedPipe;

import static org.junit.jupiter.api.Assertions.*;

@DisplayName("File system")
public class FileSystemTest {
    @Test
    @DisplayName("Move files")
    void moveFilesTest() {
        var root = Directory.create("", null);
        var namedPipe = NamedPipe.create("pipe", root);
        var homeDir = Directory.create("home", root);
        var userDir = Directory.create("user", homeDir);

        namedPipe.move("../home/user");
        assertFalse(userDir.getChildren().isEmpty());
        assertEquals(namedPipe, userDir.getChildren().get(0));

        userDir.move(root);
        assertTrue(root.getChildren().contains(userDir));
    }

    @Test
    @DisplayName("Delete files")
    void deleteFilesTest() {
        var root = Directory.create("", null);
        var homeDir = Directory.create("home", root);
        var namedPipe = NamedPipe.create("pipe", homeDir);
        var tempDir = Directory.create("user", homeDir);

        namedPipe.delete();
        assertFalse(homeDir.getChildren().contains(namedPipe));
        assertTrue(homeDir.getChildren().contains(tempDir));
        tempDir.delete();
        assertFalse(homeDir.getChildren().contains(tempDir));
        homeDir.delete();
        assertTrue(root.getChildren().isEmpty());
    }
}