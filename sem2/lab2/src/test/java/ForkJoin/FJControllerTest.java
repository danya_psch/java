package ForkJoin;

import FileSystem.Entities.BinaryFile;
import FileSystem.Entities.Directory;
import FileSystem.Entities.NamedPipe;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class FJControllerTest {

    @Test
    @DisplayName("Search")
    void search() {
        var root = Directory.create("", null);
        var sysDir = Directory.create("sys", root);
        var homeDir = Directory.create("home", root);
        var userDir = Directory.create("user", homeDir);
        var dataBin1 = BinaryFile.create("data", homeDir, "data".getBytes());
        var dataBin2 = BinaryFile.create("data", root, "data".getBytes());
        var dataBin3 = BinaryFile.create("data", userDir, "data".getBytes());
        assertEquals(root.search("data").size(), 3);
        assertTrue(root.search("data").contains("root/data"));
        assertTrue(root.search("data").contains("root/home/data"));
        assertTrue(root.search("data").contains("root/home/user/data"));

    }

    @Test
    @DisplayName("Count")
    void count() {
        var root = Directory.create("", null);
        var sysDir = Directory.create("sys", root);
        var homeDir = Directory.create("home", root);
        var userDir = Directory.create("user", homeDir);
        var userBin = BinaryFile.create("bin", homeDir, "data".getBytes());
        assertEquals(4, root.count(true));
    }

    @Test
    @DisplayName("Tree")
    void tree() {
        var root = Directory.create("", null);
        var sysDir = Directory.create("sys", root);
        var homeDir = Directory.create("home", root);
        var userDir = Directory.create("user", homeDir);
        var dataBin1 = BinaryFile.create("data", homeDir, "data".getBytes());
        var dataBin2 = BinaryFile.create("data", root, "data".getBytes());
        var dataBin3 = BinaryFile.create("data", userDir, "data".getBytes());
        JSONObject obj = new JSONObject(root.tree());
        assertEquals(obj.get("name"),  "root");
        assertEquals(
                obj.get("children").toString().replaceAll("\"", ""),
                "[{name:sys},{children:[{name:user}],name:home}]"
        );
    }
}