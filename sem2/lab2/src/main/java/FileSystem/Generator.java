package FileSystem;

import FileSystem.Actions.*;
import FileSystem.Entities.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Generator {
    public enum ActionType {
        APPEND_LINE_TO_LOG,
        CONSUME_FROM_BUFFER,
        CREATE_BINARY_FILE,
        CREATE_BUFFER_FILE,
        CREATE_DIRECTORY,
        CREATE_LOG_TEXT_FILE,
        CREATE_NAMED_PIPE,
        DELETE_ENTITY,
        GET_CHILD,
        GET_CHILDREN,
        GET_ENTITY_NAME,
        GET_ENTITY_TYPE,
        MOVE_ENTITY,
        PUSH_TO_BUFFER,
        READ_BINARY_FILE,
        READ_FROM_PIPE,
        READ_LOG_FILE,
        SET_ENTITY_NAME,
        WRITE_TO_PIPE
    }

    private final static Logger logger = Logger.getLogger(Generator.class.getName());
    private final List<Directory> directories = new ArrayList<>();
    private final List<BinaryFile> binaryFiles = new ArrayList<>();
    private final List<BufferFile<String>> bufferFiles = new ArrayList<>();
    private final List<LogTextFile> logTextFiles = new ArrayList<>();
    private final List<NamedPipe> namedPipes = new ArrayList<>();

    private final List<Future<?>> futures = new ArrayList<>();
    private final Random random = new Random();
    private int indicator = 0;

    Controller controller;
    public Generator(Controller controller) {
        try {
            this.controller = controller;
            directories.add(controller.getRoot());
            BinaryFile infBin = controller.submitFSAction(new CreateBinaryFile(controller.getRoot(), "inf", "information".getBytes())).get();
            binaryFiles.add(infBin);
            BufferFile bufBuf = controller.submitFSAction(new CreateBufferFile<String>(controller.getRoot(), "buf")).get();
            bufferFiles.add(bufBuf);
            LogTextFile eventsLog = controller.submitFSAction(new CreateLogTextFile(controller.getRoot(), "events", "events was created")).get();
            logTextFiles.add(eventsLog);
            NamedPipe somePipe = controller.submitFSAction(new CreateNamedPipe(controller.getRoot(), "some")).get();
            namedPipes.add(somePipe);
        }
        catch (Exception e) {
            logger.log(Level.WARNING, e.toString());
        }
    }

    public void performFSStressTest(int millis) throws ExecutionException, InterruptedException {
        performFSStressTest(millis, new ArrayList<>(Arrays.asList(ActionType.values())));
    }

    public void performFSStressTest(int millis, List<ActionType> actionTypes) throws ExecutionException, InterruptedException {
        long startTime = System.currentTimeMillis();
        while (System.currentTimeMillis() - startTime < millis) {
            ActionType actionType = actionTypes.get(randomInt(actionTypes.size() - 1));
                switch (actionType) {
                    case APPEND_LINE_TO_LOG: {
                        var logFile = getRandomFromList(logTextFiles);
                        var action = new AppendLineToLog(
                                logFile,
                                "line was written at " + System.currentTimeMillis()
                        );
                        futures.add(controller.submitFSAction(action));
                    }
                    continue;
                    case CONSUME_FROM_BUFFER: {
                        var bufFile = getRandomFromList(bufferFiles);
                        var action = new ConsumeFromBuffer<>(bufFile);
                        futures.add(controller.submitFSAction(action));
                    }
                    continue;
                    case CREATE_BINARY_FILE: {
                        var action = new CreateBinaryFile(
                                getRandomFromList(directories),
                                getUniqueName(Entity.EntityType.BINARY_FILE),
                                "data".getBytes()
                        );
                        var res = controller.submitFSAction(action);
                        futures.add(res);
                        binaryFiles.add(res.get());
                    }
                    continue;
                    case CREATE_BUFFER_FILE: {
                        var action = new CreateBufferFile<String>(
                                getRandomFromList(directories),
                                getUniqueName(Entity.EntityType.BUFFER_FILE)
                        );
                        var res = controller.submitFSAction(action);
                        futures.add(res);
                        bufferFiles.add(res.get());
                    }
                    continue;
                    case CREATE_DIRECTORY: {
                        var action = new CreateDirectory(
                                getRandomFromList(directories),
                                getUniqueName(Entity.EntityType.DIRECTORY)
                        );
                        var res = controller.submitFSAction(action);
                        futures.add(res);
                        directories.add(res.get());
                    }
                    continue;
                    case CREATE_LOG_TEXT_FILE: {
                        var action = new CreateLogTextFile(
                                getRandomFromList(directories),
                                getUniqueName(Entity.EntityType.LOG_TEXT_FILE),
                                "data"
                        );
                        var res = controller.submitFSAction(action);
                        futures.add(res);
                        logTextFiles.add(res.get());
                    }
                    continue;
                    case CREATE_NAMED_PIPE: {
                        var action = new CreateNamedPipe(
                                getRandomFromList(directories),
                                getUniqueName(Entity.EntityType.LOG_TEXT_FILE)
                        );
                        var res = controller.submitFSAction(action);
                        futures.add(res);
                        namedPipes.add(res.get());
                    }
                    continue;
                    case GET_CHILDREN: {
                        var action = new GetChildren(getRandomFromList(directories));
                        futures.add(controller.submitFSAction(action));
                    }
                    continue;

                    case PUSH_TO_BUFFER: {
                        var action = new PushToBuffer<String>(
                                getRandomFromList(bufferFiles),
                                "data" + System.currentTimeMillis()
                        );
                        futures.add(controller.submitFSAction(action));
                    }
                    continue;
                    case READ_BINARY_FILE: {
                        var action = new ReadBinaryFile(getRandomFromList(binaryFiles));
                        futures.add(controller.submitFSAction(action));
                    }
                    continue;
                    case READ_FROM_PIPE: {
                        var action = new ReadFromPipe(getRandomFromList(namedPipes));
                        futures.add(controller.submitFSAction(action));
                    }
                    continue;
                    case READ_LOG_FILE: {
                        var action = new ReadLogFile(getRandomFromList(logTextFiles));
                        futures.add(controller.submitFSAction(action));
                    }
                    continue;
                    case WRITE_TO_PIPE: {
                        var action = new WriteToPipe(
                                getRandomFromList(namedPipes),
                                "data" + System.currentTimeMillis()
                        );
                        futures.add(controller.submitFSAction(action));
                    } continue;
                    default: {
                        //DELETE_ENTITY, GET_CHILD, GET_ENTITY_NAME, GET_ENTITY_TYPE, MOVE_ENTITY, SET_ENTITY_NAME
                    }
                }
        }
        controller.shutDown();
    }



    private int randomInt(int max) {
        if (max == 0) return 0;
        return random.nextInt(max);
    }

    private <U> U getRandomFromList(java.util.List<U> list) {
        if (list.size() - 1 == 0) return list.get(0);
        return list.get(randomInt(list.size() - 1));
    }

    private String getUniqueName(Entity.EntityType type) {
        String name;
        switch(type) {
            case DIRECTORY: {
                name = "dir";
                break;
            }
            case BINARY_FILE: {
                name = "bin";
                break;
            }
            case LOG_TEXT_FILE: {
                name = "log";
                break;
            }
            case BUFFER_FILE: {
                name = "buf";
                break;
            }
            case NAMED_PIPE: {
                name = "pipe";
                break;
            }
            default:
                throw new IllegalStateException("Unexpected value: " + type);
        }
        name = name + System.currentTimeMillis() + indicator;
        ++indicator;
        return name;
    }
}
