package FileSystem.Actions;

import FileSystem.Entities.Entity;

public class SetEntityName extends FSAction<Void, Entity> {
    private final String newName;
    public SetEntityName(Entity target, String newName) {
        super(target);
        this.newName = newName;
    }

    @Override
    public Void perform() {
        target.setName(newName);
        return null;
    }
}
