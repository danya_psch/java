package FileSystem.Actions;

import FileSystem.Entities.Directory;

import java.util.List;

public class Search extends FSAction<List<String>, Directory> {
    private final String pattern;

    public Search(Directory directory, String pattern) {
        super(directory);
        this.pattern = pattern;
    }

    @Override
    public List<String> perform() {
        return target.search(pattern);
    }
}
