package FileSystem.Actions;

import FileSystem.Entities.NamedPipe;

public class ReadFromPipe extends FSAction<String, NamedPipe> {
    public ReadFromPipe(NamedPipe target) {
        super(target);
    }

    @Override
    public String perform() {
        return target.read();
    }
}
