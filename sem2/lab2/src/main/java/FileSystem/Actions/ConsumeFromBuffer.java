package FileSystem.Actions;

import FileSystem.Entities.BufferFile;

public class ConsumeFromBuffer<T> extends FSAction<T, BufferFile<T>> {
    public ConsumeFromBuffer(BufferFile<T> target) {
        super(target);
    }

    @Override
    public T perform() {
        return target.consume();
    }
}
