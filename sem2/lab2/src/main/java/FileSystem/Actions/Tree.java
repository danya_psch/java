package FileSystem.Actions;

import FileSystem.Entities.Directory;

import java.util.List;

public class Tree extends FSAction<String, Directory> {

    public Tree(Directory directory) {
        super(directory);
    }

    @Override
    public String perform() {
        return target.tree();
    }
}