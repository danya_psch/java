package FileSystem.Actions;

import FileSystem.Entities.*;

public abstract class CreateEntity<T> extends FSAction<T, Directory> {
    protected final String name;

    public CreateEntity(Directory directory, String name) {
        super(directory);
        this.name = name;
    }
}
