package FileSystem.Actions;

import FileSystem.Entities.LogTextFile;

public class AppendLineToLog extends FSAction<Void, LogTextFile> {
    private final String line;
    public AppendLineToLog(LogTextFile target, String line) {
        super(target);
        this.line = line;
    }

    @Override
    public Void perform() {
        target.append(line);
        return null;
    }
}
