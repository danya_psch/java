package FileSystem.Actions;

import FileSystem.Entities.BinaryFile;
import FileSystem.Entities.BufferFile;
import FileSystem.Entities.Directory;
import FileSystem.Entities.Entity;

public class CreateBufferFile<T> extends CreateEntity<BufferFile> {

    public CreateBufferFile(Directory directory, String name) {
        super(directory, name);
    }

    @Override
    public BufferFile perform() {
        return BufferFile.create(name, target);
    }
}