package FileSystem.Actions;

import FileSystem.Entities.BinaryFile;
import FileSystem.Entities.Directory;

public class Count extends FSAction<Integer, Directory> {
    private final boolean recursive;

    public Count(Directory directory, boolean recursive) {
        super(directory);
        this.recursive = recursive;
    }

    @Override
    public Integer perform() {
        return target.count(recursive);
    }
}