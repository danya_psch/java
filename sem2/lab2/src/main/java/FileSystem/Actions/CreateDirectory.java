package FileSystem.Actions;

import FileSystem.Entities.Directory;
import FileSystem.Entities.Entity;

public class CreateDirectory extends CreateEntity<Directory> {
    public CreateDirectory(Directory directory, String name) {
        super(directory, name);
    }

    public CreateDirectory() {
        super(null, null);
    }

    @Override
    public Directory perform() {
        if (target == null) return null;
        return Directory.create(name, target);
    }
}
