package FileSystem.Actions;


import FileSystem.Entities.Entity;

public abstract class FSAction<Result, TType extends Entity> {

    protected final TType target;
    public FSAction(TType target) {
        this.target = target;
    }

    public abstract Result perform();
}
