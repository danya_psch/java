package FileSystem.Actions;

import FileSystem.Entities.BinaryFile;

public class ReadBinaryFile extends FSAction<byte[], BinaryFile> {
    public ReadBinaryFile(BinaryFile target) {
        super(target);
    }

    @Override
    public byte[] perform() {
        return target.read();
    }
}
