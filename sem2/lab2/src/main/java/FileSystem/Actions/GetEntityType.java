package FileSystem.Actions;

import FileSystem.Entities.Entity;

public class GetEntityType extends FSAction<Entity.EntityType, Entity> {
    public GetEntityType(Entity target) {
        super(target);
    }

    @Override
    public Entity.EntityType perform() {
        return target.getType();
    }
}
