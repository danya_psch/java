package FileSystem.Actions;

import FileSystem.Entities.Directory;
import FileSystem.Entities.Entity;

import java.util.ArrayList;
import java.util.List;

public class GetChildren extends FSAction<List<Entity>, Directory> {
    public GetChildren(Directory directory) {
        super(directory);
    }

    @Override
    public List<Entity> perform() {
        return target.getChildren();
    }
}
