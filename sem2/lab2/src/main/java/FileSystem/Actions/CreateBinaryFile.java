package FileSystem.Actions;

import FileSystem.Entities.BinaryFile;
import FileSystem.Entities.Directory;
import FileSystem.Entities.Entity;

public class CreateBinaryFile extends CreateEntity<BinaryFile> {

    private final byte[] data;

    public CreateBinaryFile(Directory directory, String name, byte[] data) {
        super(directory, name);
        this.data = data;
    }

    @Override
    public BinaryFile perform() {
        return BinaryFile.create(name, target, data);
    }
}