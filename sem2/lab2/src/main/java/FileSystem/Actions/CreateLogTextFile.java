package FileSystem.Actions;

import FileSystem.Entities.LogTextFile;
import FileSystem.Entities.Directory;
import FileSystem.Entities.Entity;

public class CreateLogTextFile extends CreateEntity<LogTextFile> {

    private final String data;

    public CreateLogTextFile(Directory directory, String name, String data) {
        super(directory, name);
        this.data = data;
    }

    @Override
    public LogTextFile perform() {
        return LogTextFile.create(name, target, data);
    }
}