package FileSystem.Actions;

import FileSystem.Entities.BufferFile;

public class PushToBuffer<T> extends FSAction<Void, BufferFile<T>> {
    private final T data;
    public PushToBuffer(BufferFile<T> target, T data) {
        super(target);
        this.data = data;
    }

    @Override
    public Void perform() {
        target.push(data);
        return null;
    }
}
