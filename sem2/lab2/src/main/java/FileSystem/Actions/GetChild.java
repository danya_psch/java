package FileSystem.Actions;

import FileSystem.Entities.Directory;
import FileSystem.Entities.Entity;

public class GetChild extends FSAction<Entity, Directory> {
    private final String name;
    public GetChild(Directory directory, String name) {
        super(directory);
        this.name = name;
    }

    @Override
    public Entity perform() {
        return target.getChild(name);
    }
}
