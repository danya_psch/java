package FileSystem.Actions;

import FileSystem.Entities.NamedPipe;

public class WriteToPipe extends FSAction<Void, NamedPipe> {
    private final String data;
    public WriteToPipe(NamedPipe target, String data) {
        super(target);
        this.data = data;
    }

    @Override
    public Void perform() {
        target.write(data);
        return null;
    }
}
