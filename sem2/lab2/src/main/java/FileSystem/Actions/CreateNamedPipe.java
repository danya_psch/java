package FileSystem.Actions;

import FileSystem.Entities.Directory;
import FileSystem.Entities.Entity;
import FileSystem.Entities.NamedPipe;

public class CreateNamedPipe extends CreateEntity<NamedPipe> {
    public CreateNamedPipe(Directory directory, String name) {
        super(directory, name);
    }

    @Override
    public NamedPipe perform() {
        return NamedPipe.create(name, target);
    }
}