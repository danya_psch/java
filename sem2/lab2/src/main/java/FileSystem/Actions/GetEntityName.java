package FileSystem.Actions;

import FileSystem.Entities.Entity;

public class GetEntityName extends FSAction<String, Entity> {
    public GetEntityName(Entity target) {
        super(target);
    }

    @Override
    public String perform() {
        return target.getName();
    }
}
