package FileSystem.Actions;

import FileSystem.Entities.LogTextFile;

public class ReadLogFile extends FSAction<String, LogTextFile> {
    public ReadLogFile(LogTextFile target) {
        super(target);
    }

    @Override
    public String perform() {
        return target.read();
    }
}