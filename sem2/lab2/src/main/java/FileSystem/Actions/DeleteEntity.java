package FileSystem.Actions;

import FileSystem.Entities.Directory;
import FileSystem.Entities.Entity;

public class DeleteEntity extends FSAction<Void, Entity> {
    public DeleteEntity(Entity entity) {
        super(entity);
    }

    @Override
    public Void perform() {
        target.delete();
        return null;
    }
}
