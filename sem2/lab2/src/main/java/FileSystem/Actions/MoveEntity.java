package FileSystem.Actions;

import FileSystem.Entities.*;

public class MoveEntity extends FSAction<Void, Entity> {
    private enum MoveType {
        BY_DIRECTORY,
        BY_STRING
    }
    private final MoveType type;
    private Directory destination;
    private String path;

    public MoveEntity(Directory destination, Entity entity) {
        super(entity);
        this.type = MoveType.BY_DIRECTORY;
        this.destination = destination;
    }

    public MoveEntity(String path, Entity entity) {
        super(entity);
        this.type = MoveType.BY_STRING;
        this.path = path;
    }

    @Override
    public Void perform() {
        if (this.type == MoveType.BY_DIRECTORY) {
            target.move(destination);
        } else {
            target.move(path);
        }
        return null;
    }
}


