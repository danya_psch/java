package FileSystem;

import FileSystem.Actions.FSAction;
import FileSystem.Entities.Directory;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class Controller {

    private final Directory root;
    private ExecutorService workers;
    private int amountOfWorkerThreads;
    public Controller() {
        this(Runtime.getRuntime().availableProcessors());
    }
    public Controller(int amountOfWorkerThreads) {
        this.root = Directory.create("root", null);
        this.workers = Executors.newFixedThreadPool(amountOfWorkerThreads);
        this.amountOfWorkerThreads = amountOfWorkerThreads;

    }

    public void setAmountOfWorkerThreads(int val) {
        amountOfWorkerThreads = val;
        workers = Executors.newFixedThreadPool(amountOfWorkerThreads);
    }

    public void shutDown() {
        workers.shutdown();
    }

    public Directory getRoot() {
        return root;
    }

    public <T> Future<T> submitFSAction (FSAction<T, ?> action) {
        return workers.submit(action::perform);
    }
}
