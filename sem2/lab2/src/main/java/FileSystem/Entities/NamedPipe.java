package FileSystem.Entities;

import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class NamedPipe extends Entity {
    private String data;
    private final ReadWriteLock readWriteLock = new ReentrantReadWriteLock();


    private NamedPipe(String name, Directory parent) {
        super(name, parent);
        data = "";
    }

    public static NamedPipe create(String name, Directory parent) {
        return new NamedPipe(name, parent);
    }

    public void write(String data) {
        if (data == null) {
            throw new IllegalArgumentException("Data should not be null");
        }
        readWriteLock.writeLock().lock();
        try {
            this.data = data;
        }
        finally {
            readWriteLock.writeLock().unlock();
        }
    }

    public String read() {
        readWriteLock.readLock().lock();
        try {
            return data;
        }
        finally {
            readWriteLock.readLock().unlock();
        }
    }


    @Override
    public EntityType getType() {
        return EntityType.NAMED_PIPE;
    }
}
