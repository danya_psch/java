package FileSystem.Entities;

public class BinaryFile extends Entity {
    private byte[] data;

    private BinaryFile(String name, Directory parent, byte[] data) {
        super(name, parent);
        this.data = data;
    }

    public static BinaryFile create(String name, Directory parent, byte[] data) {
        if (data == null) {
            throw new IllegalArgumentException("Data should not be null");
        }
        return new BinaryFile(name, parent, data);
    }

    public byte[] read() {
        return data.clone();
    }

    @Override
    public EntityType getType() {
        return EntityType.BINARY_FILE;
    }
}