package FileSystem.Entities;

import ForkJoin.*;

import java.util.*;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.function.Consumer;
import java.util.function.Supplier;

public class Directory extends Entity {
    private static final int DIR_MAX_ELEMS = 25;
    private Map<String, Entity> children = new HashMap<>(DIR_MAX_ELEMS);

    private final ReadWriteLock readWriteLock = new ReentrantReadWriteLock();

    private Directory() {
        super("root");
    }

    private Directory(String name, Directory parent) {
        super(name, parent);
    }

    private <T> T readLock(Supplier<T> fn) {
        readWriteLock.readLock().lock();
        try { return fn.get(); }
        finally { readWriteLock.readLock().unlock(); }
    }

    private <T> T writeLock(Supplier<T> fn) {
        readWriteLock.writeLock().lock();
        try { return fn.get(); }
        finally { readWriteLock.writeLock().unlock(); }
    }

    protected <T> T readSynchronizedOperationWithTree(Supplier<T> fn) {
        forEachAncestors(dir -> dir.readWriteLock.readLock().lock());
        try { return fn.get(); }
        finally { forEachAncestors(dir -> dir.readWriteLock.readLock().unlock()); }
    }
    protected <T> T writeSynchronizedOperationWithTree(Supplier<T> fn) {
        forEachAncestors(dir -> dir.readWriteLock.writeLock().lock());
        try { return fn.get(); }
        finally { forEachAncestors(dir -> dir.readWriteLock.writeLock().unlock()); }
    }
    private void forEachAncestors(Consumer<Directory> fn) {
        Directory cur = this;
        while (cur != null) {
            fn.accept(cur);
            cur = cur.getParent();
        }
    }

    public void readSynchronizedOperationWithChildren(Runnable fn) { readLock(runnableToSupplier(fn)); }
    public void writeSynchronizedOperationWithChildren(Runnable fn) { writeLock(runnableToSupplier(fn)); }
    private <T> Supplier<T> runnableToSupplier(Runnable fn)  {
        return () -> {
            fn.run();
            return null;
        };
    }

    protected boolean containsChild(String childName) {
        return readLock(() -> children.containsKey(childName));
    }

    protected int countChildren() {
        return children.size();
    }

    protected void addChild(Entity child) {
        if (countChildren() < DIR_MAX_ELEMS) {
            children.put(child.getName(), child);
        }
    }

    protected Entity removeChild(String childName) {
        return children.remove(childName);
    }

    protected void updateChild(Entity child, String oldName) {
        children.put(child.getName(), removeChild(oldName));
    }

    private void throwExceptionIfDirIsRoot() {
        if (getParent() == null) {
            throw new IllegalCallerException("Root directory couldn't move");
        }
    }

    public static Directory create(String name, Directory parent) {
        if (parent == null) {
            return new Directory();
        }
        return new Directory(name, parent);
    }

    public Entity getChild(String childName) {
        return readLock(() -> children.get(childName));
    }

    public List<Entity> getChildren() {
        return readLock(() -> new ArrayList<>(children.values()));
    }

    public List<String> search(String pattern) {
        return ForkJoinPool.commonPool().invoke(new Search(this, pattern));
    }

    public int count(boolean recursive) {
        return ForkJoinPool.commonPool().invoke(new Count(this, recursive));
    }

    public String tree() {
        return ForkJoinPool.commonPool().invoke(new Tree(this));
    }

    @Override
    public void delete() {
        writeSynchronizedOperationWithChildren(() -> {
            if (countChildren() != 0) {
                throw new IllegalCallerException("Dir not empty");
            }
            super.delete();
        });
    }

    @Override
    public void move(Directory destination) {
        throwExceptionIfDirIsRoot();
        super.move(destination);
    }

    @Override
    public EntityType getType() {
        return EntityType.DIRECTORY;
    }

    @Override
    public boolean isDirectory() {
        return true;
    }
}
