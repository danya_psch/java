package FileSystem.Entities;

import java.util.concurrent.Semaphore;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class LogTextFile extends Entity {
    private StringBuilder data;
    private final ReadWriteLock readWriteLock = new ReentrantReadWriteLock();
    private int countReaders;

    private LogTextFile(String name, Directory parent, String data) {
        super(name, parent);
        this.data = new StringBuilder(data);
    }

    public static LogTextFile create(String name, Directory parent, String data) {
        if (data == null) {
            throw new IllegalArgumentException("Data should not be null");
        }
        return new LogTextFile(name, parent, data);
    }

    public String read() {
        readWriteLock.readLock().lock();
        try {
            return data.toString();
        }
        finally {
            readWriteLock.readLock().unlock();
        }
    }

    public void append(String line) {
        if (line == null) {
            throw new IllegalArgumentException("Line should not be null");
        }
        readWriteLock.writeLock().lock();
        try {
            data.append(line);
        }
        finally {
            readWriteLock.writeLock().unlock();
        }
    }

    @Override
    public EntityType getType() {
        return EntityType.LOG_TEXT_FILE;
    }
}