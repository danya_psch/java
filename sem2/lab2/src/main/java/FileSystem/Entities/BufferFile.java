package FileSystem.Entities;

import java.util.LinkedList;
import java.util.Queue;

public class BufferFile<T> extends Entity {
    private final static int MAX_BUF_FILE_SIZE = 10;
    private Queue<T> queue = new LinkedList<>();

    private BufferFile(String name, Directory parent) {
        super(name, parent);
    }

    public static <T> BufferFile<T> create(String name, Directory parent) {
        return new BufferFile<>(name, parent);
    }

    public synchronized void push(T element) {
        if (element == null) {
            throw new IllegalArgumentException("Element could not be null");
        }
        if (queue.size() >= MAX_BUF_FILE_SIZE) {
            throw new IllegalCallerException("Too many elements in buffer");
        }
        queue.add(element);
    }

    public synchronized T consume() {
        return queue.remove();
    }

    @Override
    public EntityType getType() {
        return EntityType.BUFFER_FILE;
    }
}
