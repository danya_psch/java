package FileSystem.Entities;

import java.util.Arrays;
import java.util.function.Consumer;
import java.util.function.Function;

public abstract class Entity {
    public enum EntityType {
        DIRECTORY,
        BINARY_FILE,
        LOG_TEXT_FILE,
        BUFFER_FILE,
        NAMED_PIPE
    }

    private Directory parent;
    private String name;

    protected Entity(String name) {
        if (name == null || name.length() == 0 || name.contains("/")) {
            throw new IllegalArgumentException("Name could not be null or zero-length and contain '/'");
        }
        this.name = name;
    }

    protected Entity(String name, Directory parent) {
        this(name);
        if (parent == null) {
            throw new IllegalArgumentException("Parent directory could not be null");
        }
        parent.writeSynchronizedOperationWithChildren(() -> {
            if (parent.containsChild(name)) {
                throw new IllegalArgumentException("Element with same name already exists");
            }
            this.parent = parent;
            parent.addChild(this);
        });
    }

    protected Directory getParent() {
        return parent;
    }

    protected Directory getRoot() {
        if (parent == null) {
            return (Directory) this;
        }
        Directory current = parent;
        while (current.getParent() != null) {
            current = current.getParent();
        }
        return current;
    }

    public void setName(String name) {
        if (name == null || name.length() == 0) {
            throw new IllegalArgumentException("Name could not be null or zero-length");
        }
        if (parent != null) {
            parent.writeSynchronizedOperationWithChildren(() -> {
                var oldName = this.name;
                this.name = name;
                parent.updateChild(this, oldName);
            });
        }
    }

    public void delete() {
        if (parent != null) {
            parent.writeSynchronizedOperationWithChildren(() -> {
                parent.removeChild(name);
                parent = null;
            });
        }
    }

    public void move(Directory destination) {
        if (destination == null) {
            throw new IllegalArgumentException("Destination could not be null");
        }
        destination.writeSynchronizedOperationWithChildren(() -> {
            if (parent != null) {
                parent.writeSynchronizedOperationWithChildren(() -> parent.removeChild(name));
            }
            parent = destination;
            parent.addChild(this);
        });
    }

    public void move(String destination) {
        if (destination == null) {
            throw new IllegalArgumentException("Destination could not be null");
        }
        var destinationEntity = parent.writeSynchronizedOperationWithTree(() -> {
            var entity = retrieveEntityFromPath(destination);
            if (!entity.isDirectory()) {
                throw new IllegalArgumentException("Destination is not a directory");
            }
            return (Directory)entity;
        });
        move(destinationEntity);
    }

    protected Entity retrieveEntityFromPath(String path) {
        String[] subFolders = path.split("/");
        Entity currDir;
        boolean startsWithDir = false;
        if (path.startsWith("/")) {
            currDir = getRoot();
        } else if (path.startsWith("..")) {
            currDir = parent;
        } else if (path.startsWith(".")) {
            currDir = this;
        } else {
            startsWithDir = true;
            currDir = this;
        }
        if (!startsWithDir) {
            subFolders = Arrays.copyOfRange(subFolders, 1, subFolders.length);
        }
        if (currDir == null) throw new IllegalArgumentException("Path not valid, expected valid path");
        for (var nextDir : subFolders) {
            if (!currDir.isDirectory()) throw new IllegalArgumentException("Path not valid, expected directory");
            currDir = ((Directory)currDir).getChild(nextDir);
        }
        return currDir;
    }

    public String getFullName() {
        if (parent != null) {
            return parent.readSynchronizedOperationWithTree(() -> {
                StringBuilder fullName = new StringBuilder();
                fullName.insert(0, this.getName());
                Entity cur = getParent();
                while (cur != null) {
                    fullName.insert(0, cur.getName() + "/");
                    cur = cur.getParent();
                }
                return fullName.toString();
            });
        }
        return "root";
    }

    public String getName() {
        return name;
    }

    public abstract EntityType getType();

    public boolean isDirectory() {
        return false;
    }
}
