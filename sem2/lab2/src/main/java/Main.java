import FileSystem.Controller;
import FileSystem.Actions.*;
import FileSystem.Entities.*;
import FileSystem.Generator;

public class Main {
    public static void main(String[] args) {
        try {
            Controller controller = new Controller();
            Generator generator = new Generator(controller);
            generator.performFSStressTest(1000);
            fsTraverse(controller.getRoot(), 0);
        }
        catch (Exception e) {
            System.out.println(e.toString());
        }
    }


    private static void fsTraverse(Directory dir, int indent) {
        System.out.println(" ".repeat(indent) + entityPrint(dir));
        indent += 4;
        for (var cur : dir.getChildren()) {
            if (cur.isDirectory()) {
                fsTraverse((Directory) cur, indent);
            } else {
                System.out.println(" ".repeat(indent) + entityPrint(cur));
            }
        }
    }

    private static String entityPrint(Entity e) {
        return e.getType().toString() + ": " + e.getName();
    }
}
