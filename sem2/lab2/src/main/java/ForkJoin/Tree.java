package ForkJoin;

import FileSystem.Entities.Entity;
import org.json.JSONArray;
import org.json.JSONObject;
import FileSystem.Entities.Directory;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.RecursiveTask;

public class Tree extends RecursiveTask<String> {
    final private Directory directory;


    public Tree(Directory directory) {
        this.directory = directory;
    }

    @Override
    protected String compute() {
        JSONObject obj = new JSONObject();
        directory.readSynchronizedOperationWithChildren(() -> {
            List<Tree> tasks = new ArrayList<>();
            List<Entity> children = directory.getChildren();
            obj.put("name", directory.getName());
            for (Entity child : children) {
                if (child.isDirectory()) {
                    Tree task = new Tree((Directory) child);
                    task.fork();
                    tasks.add(task);
                }
            }
            addResultsFromTasks(obj, tasks);

        });

        return obj.toString().replaceAll("\"", "");
    }

    private void addResultsFromTasks(JSONObject obj, List<Tree> tasks)
    {
        JSONArray arr = new JSONArray();
        if (tasks.size() == 0) return;
        for (Tree item : tasks)
        {
            arr.put(item.join());
        }
        obj.put("children", arr);
    }
}
