package ForkJoin;

import FileSystem.Entities.Directory;
import FileSystem.Entities.Entity;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.RecursiveTask;
import java.util.concurrent.atomic.AtomicInteger;

public class Count extends RecursiveTask<Integer> {
    private final boolean recursive;
    private final Directory directory;

    public Count(Directory directory, boolean recursive) {
        this.recursive = recursive;
        this.directory = directory;
    }
    @Override
    protected Integer compute() {
        AtomicInteger numberOfFiles = new AtomicInteger();
        directory.readSynchronizedOperationWithChildren(() -> {
            List<Count> tasks = new ArrayList<>();
            List<Entity> children = directory.getChildren();
            numberOfFiles.addAndGet(children.size());
            if (recursive) {
                for (Entity child : children) {
                    if (child.isDirectory()) {
                        Count task = new Count((Directory) child, recursive);
                        task.fork();
                        tasks.add(task);
                    }
                }
            }
            numberOfFiles.set(addResultsFromTasks(numberOfFiles.get(), tasks));

        });
        return numberOfFiles.get();
    }

    private int addResultsFromTasks(int numberOfFiles, List<Count> tasks)
    {
        for (Count item : tasks)
        {
            numberOfFiles += item.join();
        }
        return numberOfFiles;
    }
}
