package ForkJoin;

import FileSystem.Entities.Directory;
import FileSystem.Entities.Entity;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.RecursiveTask;

public class Search extends RecursiveTask<List<String>> {
    private final String pattern;
    private final Directory directory;

    public Search(Directory directory, String pattern) {
        this.directory = directory;
        this.pattern = pattern;
    }

    @Override
    protected List<String> compute() {
        List<String> list = new ArrayList<>();
        directory.readSynchronizedOperationWithChildren(() -> {
            List<Search> tasks = new ArrayList<>();
            List<Entity> children = directory.getChildren();
            for (Entity child : children) {
                if (child.isDirectory()) {
                    Search search = new Search((Directory) child, pattern);
                    search.fork();
                    tasks.add(search);
                } else if (child.getName().equals(pattern)) {
                    list.add(child.getFullName());
                }
            }
            addResultsFromTasks(list, tasks);
        });
        return list;
    }

    private void addResultsFromTasks(List<String> list, List<Search> tasks)
    {
        for (Search item : tasks)
        {
            list.addAll(item.join());
        }
    }
}
