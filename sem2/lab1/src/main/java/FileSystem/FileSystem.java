package FileSystem;

import FileSystem.Files.Directory;
import FileSystem.Files.File;
import FileSystem.Files.ReferenceFile;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.locks.ReentrantLock;

public class FileSystem {
    public enum Type {
        Directory,
        Binary,
        Buffer,
        LogText,
        Stack,
        ReferenceFile
    }

    private Directory curr_dir_;

    static private Directory root_ = new Directory("~", null);
    private File fileForMove = null;

    static private HashMap<Long, File> filesForMove = new HashMap<>();

    public FileSystem() {
//        root_ = new Directory("~", null);
        FileSystem.clean();
        curr_dir_ = root_;
    }

    public void insertFile(File file) {
        if (file.getParent() != null) return;
        curr_dir_.insert(file);
    }

    public File removeFile(String name) {
        return curr_dir_.remove(name);
    }
    public File removeFileFromParent(File file) {
        if (file == null || file.getParent() == null) return null;
        return file.getParent().remove(file.getName());
    }

    public synchronized static boolean setFileForMove(FileSystem fs, String name){
        File file_in_question = fs.curr_dir_.getListOfFiles().get(name);
        if (!filesForMove.containsValue(file_in_question)) {
            filesForMove.put(Thread.currentThread().getId(), file_in_question);
            fs.fileForMove = file_in_question;
            return true;
        }
        return false;
    }

    public synchronized static void putFileForMove(FileSystem fs) {
        fs.removeFileFromParent(fs.fileForMove);
        fs.insertFile(fs.fileForMove);
        filesForMove.remove(Thread.currentThread().getId());
        fs.fileForMove = null;
    }

    public void cd(String name) {
        if (name.equals("..") && curr_dir_ != root_) {
            curr_dir_ = curr_dir_.getParent();
        } else {
            File file = curr_dir_.getListOfFiles().get(name);
            if (/*file != null && */ file instanceof Directory) {
                curr_dir_ = (Directory) file;
            }
        }
    }
    public HashMap<String, File> ls() {
        return curr_dir_.getListOfFiles();
    }
    public String getCurrPath() {
        Directory curr_dir = curr_dir_;
        String path = "";
        while(curr_dir != null) {
            path = curr_dir.getName() + "/" + path;
            curr_dir = curr_dir.getParent();
        }
        return path;
    }

    public void print() {
        printFile(root_, 0);
    }
    private void printFile(File file, int level) {
        if (file == null) return;

        StringBuilder str = new StringBuilder();
        for (int i = 0; i < level; ++i) {
            str.append("..");
        }

        System.out.println(str + file.getName() + ": " + file.getClass().toString());
        if (file instanceof Directory) {
            for(Map.Entry<String, File> entry : ((Directory) file).getListOfFiles().entrySet()) {
                printFile(entry.getValue(), level + 1);
            }
        }
    }

    public File getFile(String name) {
        return curr_dir_.getListOfFiles().get(name);
    }

    public static void clean() {
        root_ = new Directory("~", null);
    }
}
