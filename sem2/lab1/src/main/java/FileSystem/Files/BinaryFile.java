package FileSystem.Files;

import FileSystem.FileSystem;

public class BinaryFile extends File {
    private String data_;
    public BinaryFile(String name, String data) {
        super(name, FileSystem.Type.Binary);
        data_ = data;
    }

    public String readFile() {
        return data_;
    }
}
