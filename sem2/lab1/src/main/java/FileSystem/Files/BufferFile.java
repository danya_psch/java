package FileSystem.Files;

import FileSystem.FileSystem;

import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.locks.ReentrantLock;

public class BufferFile<T> extends File {
    public static final int MAX_BUF_FILE_SIZE = 30;
    private Queue<T> queue_ = new LinkedList<>();
    private ReentrantLock mutex = new ReentrantLock();

    public BufferFile(String name) {
        super(name, FileSystem.Type.Buffer);
    }

    public BufferFile push(T elem) {
        if (queue_.size() >= MAX_BUF_FILE_SIZE) return null;
        mutex.lock();
        try {
            queue_.add(elem);
            return this;
        } finally {
            mutex.unlock();
        }
    }

    public T pop() {
        if (queue_.size() == 0) return null;
        mutex.lock();
        try {
            return queue_.remove();
        } finally {
            mutex.unlock();
        }
    }

    public int getSize() {
        return queue_.size();
    }
}
