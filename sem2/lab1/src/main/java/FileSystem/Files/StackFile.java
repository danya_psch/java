package FileSystem.Files;

import FileSystem.FileSystem;

import java.util.Stack;
import java.util.concurrent.locks.ReentrantLock;

public class StackFile<T> extends File {
    public static final int MAX_STACK_FILE_SIZE = 30;
    private Stack<T> stack_;
    private ReentrantLock mutex = new ReentrantLock();
    public StackFile(String name) {
        super(name, FileSystem.Type.Stack);
        stack_ =  new Stack<T>();
    }

    public StackFile push(T elem) {
        if (stack_.size() == MAX_STACK_FILE_SIZE) return null;
        mutex.lock();
        try {
            stack_.push(elem);
            return this;
        } finally {
            mutex.unlock();
        }

    }

    public T pop() {
        if (stack_.size() == 0) return null;
        mutex.lock();
        try {
            return stack_.pop();
        } finally {
            mutex.unlock();
        }
    }

    public int size() {
        return stack_.size();
    }


}
