package FileSystem.Files;

import FileSystem.FileSystem;

import java.util.concurrent.locks.ReentrantLock;

public class LogTextFile extends File {
    private String data_ = "";
    private ReentrantLock mutex = new ReentrantLock();
    public LogTextFile(String name) {
        super(name, FileSystem.Type.LogText);
    }

    public String readFile() {
        return data_;
    }

    public LogTextFile appendLine(String line) {
        mutex.lock();
        try {
            data_ += line + '\n';
            return this;
        } finally {
            mutex.unlock();
        }
    }
}
