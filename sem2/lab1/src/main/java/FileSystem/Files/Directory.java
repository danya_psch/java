package FileSystem.Files;

import FileSystem.FileSystem;

import java.util.HashMap;
import java.util.concurrent.locks.ReentrantLock;

public class Directory extends File {
    public static final int DIR_MAX_ELEMS = 20;
    private HashMap <String, File> files_ = new HashMap<>();
    private ReentrantLock mutex = new ReentrantLock();

    public Directory(String name) {
        super(name, FileSystem.Type.Directory);
    }

    public Directory(String name, Directory parent) {
        super(name, FileSystem.Type.Directory, parent);
    }

    public void insert(File file) {
        mutex.lock();
        try {
            if (files_.size() == DIR_MAX_ELEMS) return;
            String name = file.getName();
            if (files_.containsKey(name)) return;

            files_.put(name, file);
            file.setParent(this);
        } finally {
            mutex.unlock();
        }
    }

    public File remove(String name) {
        mutex.lock();
        try {
            if (
                    files_.size() == 0 ||
                    !files_.containsKey(name) ||
                    files_.get(name).getParent() == null
            ) return null;

            files_.get(name).setParent(null);
            return files_.remove(name);
        } finally {
            mutex.unlock();
        }
    }

    public HashMap<String, File> getListOfFiles() {
        return files_;
    }
}
