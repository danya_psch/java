package FileSystem.Files;

import FileSystem.FileSystem;

public class ReferenceFile extends File {
    private File file_;
    public ReferenceFile(String name, File file) {
        super(name, FileSystem.Type.ReferenceFile);
        file_ = file;
        file_.addReference(this);
    }

    public File getFile() {
        return file_;
    }
}