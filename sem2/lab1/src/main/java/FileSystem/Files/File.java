package FileSystem.Files;

import FileSystem.FileSystem;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public abstract class File {

    private String name_;
    private FileSystem.Type type_;
    private Directory parent_;
    private List<ReferenceFile>  listOfReferences = new ArrayList();

    ReadWriteLock readWriteLock1 = new ReentrantReadWriteLock();
    ReadWriteLock readWriteLock2 = new ReentrantReadWriteLock();
    ReadWriteLock readWriteLock3 = new ReentrantReadWriteLock();

    public File(String name, FileSystem.Type type) {
        name_ = name;
        type_ = type;
    }

    public File(String name, FileSystem.Type type, Directory parent) {
        name_ = name;
        type_ = type;
        parent_ = parent;
    }

    public String getName() {
        readWriteLock1.readLock().lock();
        try {
            return name_;
        } finally {
            readWriteLock1.readLock().unlock();
        }
    }

    public void setName(String name) {
        readWriteLock1.writeLock().lock();
        name_ = name;
        readWriteLock1.writeLock().unlock();
    }

    public Directory getParent() {
        readWriteLock2.readLock().lock();
        try {
            return parent_;
        } finally {
            readWriteLock2.readLock().unlock();
        }
    }

    public void setParent(Directory parent) {

        readWriteLock2.writeLock().lock();
        parent_ = parent;
        readWriteLock2.writeLock().unlock();
    }

    public FileSystem.Type getType() {
        return type_;
    }

    public List<ReferenceFile> getReferences() {
        readWriteLock3.readLock().lock();
        try {
            return listOfReferences;
        } finally {
            readWriteLock3.readLock().unlock();
        }
    }

    protected void addReference(ReferenceFile refFile) {
        readWriteLock3.writeLock().lock();
        listOfReferences.add(refFile);
        readWriteLock3.writeLock().unlock();
    }
}
