import FileSystem.*;
import FileSystem.Files.*;

import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.CyclicBarrier;


class Global {

    public static CountDownLatch latch_1 = new CountDownLatch(2);
    public static CountDownLatch latch_2 = new CountDownLatch(4);
}

class MyThread implements Runnable
{
    private Thread t_;
    private CyclicBarrier br_;
    private long time1_, time2_, time3_;
    private Type type_;

    enum Type {
        First,
        Second,
        Third
    }

    MyThread(String name, long time1, long time2, long time3, CyclicBarrier br, Type type)
    {
        type_ = type;
        time1_ = time1;
        time2_ = time2;
        time3_ = time3;
        t_  = new Thread(this, name);
        br_ = br;
        t_.start();
    }

    public void run()
    {
        try {
            task_init();
            System.out.println(t_.getName() + ": task_init was finished\n");
            br_.await();
            task();
            System.out.println(t_.getName() + ": task was finished\n");
            if (type_ == Type.Second) {
                Global.latch_1.await();
            }
            task_finalize();
            switch(type_) {
                case First: {
                    Global.latch_1.countDown();
                }
                case Second: {
                    Global.latch_2.countDown();
                    break;
                }
                case Third: {
                    Global.latch_2.await();
                    break;
                }
            }
            System.out.println(t_.getName() + " was finished\n");
        } catch (InterruptedException | BrokenBarrierException e) {
            e.printStackTrace();
        }
    }

    private void task_init() throws InterruptedException {
        System.out.println(t_.getName() + ": start task_init\n");
        Thread.sleep(time1_);
    }

    private void task() throws InterruptedException{
        System.out.println(t_.getName() + ": start task\n");
        Thread.sleep(time2_);
    }

    private void task_finalize() throws InterruptedException{
        System.out.println(t_.getName() + ": start task_finalize\n");
        Thread.sleep(time3_);
    }
}

public class Main {
    public static void main(String[] args) {
        task2();
        task3();
    }

    public static void task2() {
        System.out.println("\ntask2////////////////////////////////////////////////////////////////////////////////\n");
        FileSystem fs = new FileSystem();
        fs.insertFile(new BinaryFile("bin1", "data"));
        fs.insertFile(new Directory("dir1"));
        fs.insertFile(new Directory("dir2"));
        fs.cd("dir1");
        fs.insertFile(new BufferFile<String>("buf1"));
        fs.insertFile(new Directory("dir11"));
        fs.insertFile(new Directory("dir12"));
        fs.cd("dir11");
        StackFile<String> stack1 = new StackFile<String>("stack1");
        stack1.push("el1").push("el2");
        StackFile<String> stack2 = new StackFile<String>("stack2");
        stack2.push("el1").push("el2").push("el3");
        fs.insertFile(stack1);
        fs.insertFile(stack2);
        fs.cd("..");
        fs.cd("..");
        fs.cd("dir2");
        fs.insertFile(new BinaryFile("bin2", "data"));
        LogTextFile log1 = new LogTextFile("log1");
        log1.appendLine("line1").appendLine("line2");
        fs.insertFile(log1);
        fs.insertFile(new Directory("dir21"));
        fs.cd("dir21");
        fs.insertFile(new BufferFile("buf2"));
        LogTextFile log2 = new LogTextFile("log2");
        log2.appendLine("line1").appendLine("line2");
        fs.insertFile(log2);
        fs.print();
    }

    public static void task3() {
        System.out.println("\ntask3////////////////////////////////////////////////////////////////////////////////\n");
        CyclicBarrier cb = new CyclicBarrier(5);
        MyThread mt1 = new MyThread("Thread1", 200, 1000, 1000,  cb, MyThread.Type.Second);
        MyThread mt2 = new MyThread("Thread2", 400, 800, 200,  cb, MyThread.Type.Second);
        MyThread mt3 = new MyThread("Thread3", 600, 600, 800,  cb, MyThread.Type.First);
        MyThread mt4 = new MyThread("Thread4", 800, 400, 400,  cb, MyThread.Type.First);
        MyThread mt5 = new MyThread("Thread5", 1000, 200, 600,  cb, MyThread.Type.Third);

    }
}
