package FileSystem;

import FileSystem.Files.*;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class FileSystemTest {

    @Test
    void createFile() {
        FileSystem fs = new FileSystem();
        fs.insertFile(new BinaryFile("bin", "data"));
        fs.insertFile(new Directory("dir"));
        fs.insertFile(new BufferFile<String>("buf"));
        fs.insertFile(new StackFile<>("stack"));
        assertEquals(fs.ls().size(), 4);
        assertNotNull(fs.ls().get("bin"));
        assertNotNull(fs.ls().get("dir"));
        assertNotNull(fs.ls().get("buf"));
        assertNotNull(fs.ls().get("stack"));
    }


    @Test
    void insertFile() {
        FileSystem fs = new FileSystem();
        File log1 = new LogTextFile("log1");
        File log2 = new LogTextFile("log2");
        fs.insertFile(log1);
        fs.insertFile(log2);
        assertEquals(fs.ls().size(), 2);
        assertNotNull(fs.getFile("log1"));
        assertNotNull(fs.getFile("log2"));
        fs.insertFile(log2);
        assertEquals(fs.ls().size(), 2);
    }

    @Test
    void removeFile() {
        FileSystem fs = new FileSystem();
        fs.insertFile(new BinaryFile("bin", "data"));
        fs.insertFile(new Directory("dir"));
        fs.insertFile(new BufferFile<String>("buf"));
        fs.insertFile(new StackFile<>("stack"));
        assertEquals(fs.ls().size(), 4);
        fs.removeFile("buf");
        assertNull(fs.ls().get("buf"));
        fs.removeFile("stack");
        assertNull(fs.ls().get("stack"));
    }

    @Test
    void putFileForMove() {
        FileSystem fs = new FileSystem();
        fs.insertFile(new Directory("dir"));
        fs.cd("dir");
        fs.insertFile(new BinaryFile("bin", "data"));
        assertEquals(fs.ls().size(), 1);
        fs.setFileForMove(fs, "bin");
        fs.cd("..");
        fs.putFileForMove(fs);
        assertEquals(fs.ls().size(), 2);
    }

    @Test
    void cd() {
        FileSystem fs = new FileSystem();
        fs.insertFile(new Directory("dir"));
        assertEquals(fs.getCurrPath(), "~/");
        fs.cd("dir");
        assertEquals(fs.getCurrPath(), "~/dir/");
    }

    @Test
    void ls() {
        FileSystem fs = new FileSystem();
        fs.insertFile(new Directory("dir"));
        fs.cd("dir");
        fs.insertFile(new BinaryFile("bin", "data"));
        fs.insertFile(new Directory("dir"));
        fs.insertFile(new BufferFile<String>("buf"));
        fs.insertFile(new StackFile<>("stack"));
        assertEquals(fs.ls().size(), 4);
    }

    @Test
    void getFile() {
        FileSystem fs = new FileSystem();
        fs.insertFile(new Directory("dir"));
        fs.cd("dir");
        fs.insertFile(new BinaryFile("bin", "data"));
        fs.insertFile(new Directory("dir"));
        fs.insertFile(new BufferFile<String>("buf"));
        fs.insertFile(new StackFile<>("stack"));
        File buf = fs.getFile("buf");
        assertTrue(buf instanceof BufferFile);
        File bin = fs.getFile("bin");
        assertTrue(bin instanceof BinaryFile);
    }
}