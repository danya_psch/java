package FileSystem.Files;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DirectoryTest {

    @Test
    void insert() {
        Directory dir = new Directory("dir1");
        BinaryFile bin = new BinaryFile("bin", "data");
        dir.insert(bin);
        StackFile stack = new StackFile("stack");
        dir.insert(stack);
        assertEquals(dir.getListOfFiles().size(), 2);
        assertEquals(dir.getListOfFiles().get("bin"), bin);
        assertEquals(dir.getListOfFiles().get("stack"), stack);
    }

    @Test
    void remove() {
        Directory dir = new Directory("dir1");
        BinaryFile bin = new BinaryFile("bin", "data");
        dir.insert(bin);
        StackFile stack = new StackFile("stack");
        dir.insert(stack);
        assertEquals(dir.getListOfFiles().size(), 2);
        dir.remove("bin");
        assertEquals(dir.getListOfFiles().size(), 1);
        assertNull(dir.getListOfFiles().get("bin"));
    }

    @Test
    void getListOfFiles() {
        Directory dir = new Directory("dir1");
        BinaryFile bin = new BinaryFile("bin", "data");
        dir.insert(bin);
        assertEquals(dir.getListOfFiles().size(), 1);
        StackFile stack = new StackFile("stack");
        dir.insert(stack);
        assertEquals(dir.getListOfFiles().size(), 2);
    }
}