package FileSystem.Files;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class BufferFileTest {

    @Test
    void push() {
        BufferFile bfile = new BufferFile("buf");
        bfile.push("1");
        bfile.push("2");
        assertEquals(bfile.getSize(), 2);
    }

    @Test
    void pop() {
        BufferFile bfile = new BufferFile("buf");

        bfile.push("1");
        bfile.push("2");
        assertEquals(bfile.pop(), "1");
        assertEquals(bfile.pop(), "2");
    }
}