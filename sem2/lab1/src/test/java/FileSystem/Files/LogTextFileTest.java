package FileSystem.Files;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class LogTextFileTest {

    @Test
    void readFile() {
        LogTextFile lt = new LogTextFile("lt");
        lt.appendLine("line1line1line1line1line1line1line1line1line1");
        lt.appendLine("line2line2line2line2line2line2line2line2line2");
        assertEquals(lt.readFile(), "line1line1line1line1line1line1line1line1line1\nline2line2line2line2line2line2line2line2line2\n");
    }

    @Test
    void appendLine() {
        LogTextFile lt = new LogTextFile("lt");
        lt.appendLine("line1line1line1line1line1line1line1line1line1");
        assertEquals(lt.readFile(), "line1line1line1line1line1line1line1line1line1\n");
    }
}