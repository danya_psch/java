package FileSystem.Files;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class BinaryFileTest {

    @Test
    public void readFile() {
        BinaryFile bfile = new BinaryFile("bin", "datadata");
        assertEquals(bfile.readFile(), "datadata");
    }
}