package FileSystem.Files;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class StackFileTest {

    @Test
    void push() {
        StackFile sfile = new StackFile("stack");
        sfile.push("1");
        assertEquals(sfile.size(), 1);
        sfile.push("2");
        assertEquals(sfile.size(), 2);
    }

    @Test
    void pop() {
        StackFile sfile = new StackFile("stack");
        sfile.push("1");
        sfile.push("2");
        assertEquals(sfile.size(), 2);
        assertEquals(sfile.pop(), "2");
        assertEquals(sfile.size(), 1);
    }
}